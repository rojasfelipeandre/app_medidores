package com.example.app_condominios.Database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.app_condominios.Database.entity.MedicionDepartEntity2;

import java.util.List;

@Dao
public interface MedicionDepartDao2 {

    @Insert
    void insert(MedicionDepartEntity2 MedicionDepartDao);

    @Query("DELETE FROM MedicionDepartEntity2")
    void deleteAll();

    @Query("UPDATE MedicionDepartEntity2 SET is_closed = :isclosed WHERE measurement_id =:measurement_id")
    void queryUpdate2(int isclosed, int measurement_id);

    @Query("SELECT * FROM MedicionDepartEntity2")
    List<MedicionDepartEntity2> getAll();
}
