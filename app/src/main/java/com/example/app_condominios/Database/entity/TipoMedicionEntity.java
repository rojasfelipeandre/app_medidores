package com.example.app_condominios.Database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


import com.example.app_condominios.Models.Response.ResponseMedicionTipo;

@Entity
public class TipoMedicionEntity {
    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "measurement_type_id")
    private int measurement_type_id;

    @ColumnInfo(name = "code_measurement")
    private String code_measurement;

    @ColumnInfo(name = "name_measurement_type")
    private  String name_measurement_type;

    @ColumnInfo(name = "image_measurement")
    private  String image_measurement;

    public TipoMedicionEntity() {}

    public TipoMedicionEntity(ResponseMedicionTipo medicionTipo, int uid) {
        this.uid = uid;
        this.measurement_type_id = medicionTipo.getMeasurement_type_id();
        this.code_measurement = medicionTipo.getCode_measurement();
        this.name_measurement_type = medicionTipo.getName_measurement_type();
        this.image_measurement = medicionTipo.getImage_measurement();
    }


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getMeasurement_type_id() {
        return measurement_type_id;
    }

    public void setMeasurement_type_id(int measurement_type_id) {
        this.measurement_type_id = measurement_type_id;
    }

    public String getCode_measurement() {
        return code_measurement;
    }

    public void setCode_measurement(String code_measurement) {
        this.code_measurement = code_measurement;
    }

    public String getName_measurement_type() {
        return name_measurement_type;
    }

    public void setName_measurement_type(String name_measurement_type) {
        this.name_measurement_type = name_measurement_type;
    }

    public String getImage_measurement() {
        return image_measurement;
    }

    public void setImage_measurement(String image_measurement) {
        this.image_measurement = image_measurement;
    }
}
