package com.example.app_condominios.Database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.app_condominios.Models.Response.ResponseEdificios;

@Entity
public class EdificioEntity {

    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "building_id")
    private int building_id;

    @ColumnInfo(name = "name_building")
    private String name_building;

    @ColumnInfo(name = "image_building")
    private  String image_building;

    @ColumnInfo(name = "address")
    private  String address;

    public EdificioEntity() {}

    public EdificioEntity(ResponseEdificios edificios,int uid) {
        this.uid = uid;
        this.building_id = edificios.getBuilding_id();
        this.name_building = edificios.getName_building();
        this.image_building = edificios.getImage_building();
        this.address = edificios.getAddress();
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(int building_id) {
        this.building_id = building_id;
    }

    public String getName_building() {
        return name_building;
    }

    public void setName_building(String name_building) {
        this.name_building = name_building;
    }

    public String getImage_building() {
        return image_building;
    }

    public void setImage_building(String image_building) {
        this.image_building = image_building;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "EdificioEntity{" +
                "building_id=" + building_id +
                ", name_building='" + name_building + '\'' +
                ", image_building='" + image_building +
                '}';
    }

}
