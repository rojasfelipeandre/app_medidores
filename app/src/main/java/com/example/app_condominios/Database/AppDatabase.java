package com.example.app_condominios.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.app_condominios.Database.dao.EdificiosDao;
import com.example.app_condominios.Database.dao.MedicionDepartDao;
import com.example.app_condominios.Database.dao.MedicionDepartDao2;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.dao.TipoMedicionDao;
import com.example.app_condominios.Database.entity.EdificioEntity;
import com.example.app_condominios.Database.entity.MedicionDepartEntity;
import com.example.app_condominios.Database.entity.MedicionDepartEntity2;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Database.entity.TipoMedicionEntity;

@Database(entities = {EdificioEntity.class, TipoMedicionEntity.class, MedicionDepartEntity.class, MedicionDepartEntity2.class, RegistroEntity.class},
        version = 3, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "medidores-database";
    private static AppDatabase mInstance;

    public synchronized static AppDatabase getDatabaseInstance(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return mInstance;
    }

    public abstract EdificiosDao edificiosDao();
    public abstract TipoMedicionDao tipoMedicionDao();
    public abstract MedicionDepartDao medicionDepartDao();
    public abstract MedicionDepartDao2 medicionDepartDao2();
    public abstract RegistroDao registroDao();

}
