package com.example.app_condominios.Database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.app_condominios.Database.entity.TipoMedicionEntity;

import java.util.List;

@Dao
public interface TipoMedicionDao {

    @Insert
    void insert(TipoMedicionEntity TipoMedicionDao);

    @Query("DELETE FROM TipoMedicionEntity")
    void deleteAll();

    @Query("SELECT * FROM TipoMedicionEntity")
    List<TipoMedicionEntity> getAll();
}
