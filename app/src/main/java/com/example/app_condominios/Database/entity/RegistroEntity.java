package com.example.app_condominios.Database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.app_condominios.Models.Request.RequestUpdateRegistro;

@Entity
public class RegistroEntity {

    @PrimaryKey (autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "measurementid")
    private int measurementid;

    @ColumnInfo(name = "reading_month_now")
    private double reading_month_now;

    @ColumnInfo(name = "image_attached")
    private String image_attached;

    //1 = Pendiente de subida - 2: Subido al servidor
    @ColumnInfo(name = "estado")
    private int estado;

    public RegistroEntity() { }

    public RegistroEntity (RequestUpdateRegistro registro, int measurementid, int estado) {
        this.measurementid = measurementid;
        this.estado = estado;
        reading_month_now = registro.getReading_month_now();
        image_attached = registro.getImage_attached();
    }


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getMeasurementid() {return measurementid; }

    public void setMeasurementid(int measurementid) {
        this.measurementid = measurementid;
    }

    public double getReading_month_now() {
        return reading_month_now;
    }

    public void setReading_month_now(double reading_month_now) {
        this.reading_month_now = reading_month_now;
    }

    public String getImage_attached() {
        return image_attached;
    }

    public void setImage_attached(String image_attached) {
        this.image_attached = image_attached;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
