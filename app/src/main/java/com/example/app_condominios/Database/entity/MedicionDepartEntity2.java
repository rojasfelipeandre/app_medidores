package com.example.app_condominios.Database.entity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.app_condominios.Models.Response.ResponseMedicionDepart;

@Entity
public class MedicionDepartEntity2 {


    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "measurement_id")
    private int measurement_id;

    @ColumnInfo(name = "number_estate")
    private String number_estate;

    @ColumnInfo(name = "reading_month_past")
    private  String reading_month_past;

    @ColumnInfo(name = "reading_month_now")
    private  String reading_month_now;

    @ColumnInfo(name = "image_attached")
    private  String image_attached;

    @ColumnInfo(name = "is_closed")
    private  int is_closed;


    public MedicionDepartEntity2() {}

    public MedicionDepartEntity2(ResponseMedicionDepart medicionDepart, int uid) {
        this.uid = uid;
        this.measurement_id = medicionDepart.getMeasurement_id();
        this.number_estate = medicionDepart.getNumber_estate();
        this.reading_month_past = medicionDepart.getReading_month_past();
        this.reading_month_now = medicionDepart.getReading_month_now();
        this.image_attached = medicionDepart.getImage_attached();
        this.is_closed = medicionDepart.getIs_closed();
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getMeasurement_id() {
        return measurement_id;
    }

    public void setMeasurement_id(int measurement_id) {
        this.measurement_id = measurement_id;
    }

    public String getNumber_estate() {
        return number_estate;
    }

    public void setNumber_estate(String number_estate) {
        this.number_estate = number_estate;
    }

    public String getReading_month_past() {
        return reading_month_past;
    }

    public void setReading_month_past(String reading_month_past) {
        this.reading_month_past = reading_month_past;
    }

    public String getReading_month_now() {
        return reading_month_now;
    }

    public void setReading_month_now(String reading_month_now) {
        this.reading_month_now = reading_month_now;
    }

    public String getImage_attached() {
        return image_attached;
    }

    public void setImage_attached(String image_attached) {
        this.image_attached = image_attached;
    }

    public int getIs_closed() {
        return is_closed;
    }

    public void setIs_closed(int is_closed) {
        this.is_closed = is_closed;
    }
}
