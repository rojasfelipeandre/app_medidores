package com.example.app_condominios.Database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.app_condominios.Database.entity.EdificioEntity;

import java.util.List;

@Dao
public interface EdificiosDao {

    @Insert
    void insert(EdificioEntity EdificioDao);

    @Query("DELETE FROM EdificioEntity")
    void deleteAll();

    @Query("SELECT * FROM EdificioEntity")
    List<EdificioEntity> getAll();

}
