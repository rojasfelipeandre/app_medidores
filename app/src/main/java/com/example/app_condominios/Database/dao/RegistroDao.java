package com.example.app_condominios.Database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.app_condominios.Database.entity.RegistroEntity;

import java.util.List;


@Dao
public interface RegistroDao {

    @Insert
    void insert(RegistroEntity RegistroDao);

    @Delete
    void delete(RegistroEntity RegistroDao);


    @Query("UPDATE RegistroEntity SET estado = :estado WHERE uid =:uid")
    void queryUpdate(int estado, int uid);

    @Query("SELECT * FROM RegistroEntity")
    List<RegistroEntity> getAll();

    /*@Query("SELECT * FROM RegistroEntity WHERE uid = :uid")
    List<RegistroEntity> getAllByTipoEvento(int uid);*/

    @Query("DELETE FROM RegistroEntity")
    void deleteAll();

    @Query("DELETE FROM RegistroEntity WHERE uid =:uid")
    void queryDelete(int uid);

}
