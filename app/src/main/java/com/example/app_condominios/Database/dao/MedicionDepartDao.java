package com.example.app_condominios.Database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.app_condominios.Database.entity.MedicionDepartEntity;

import java.util.List;

@Dao
public interface MedicionDepartDao {

    @Insert
    void insert(MedicionDepartEntity MedicionDepartDao);

    @Query("DELETE FROM MedicionDepartEntity")
    void deleteAll();

    @Query("UPDATE MedicionDepartEntity SET is_closed = :isclosed WHERE measurement_id =:measurement_id")
    void queryUpdate(int isclosed, int measurement_id);

    @Query("SELECT * FROM MedicionDepartEntity")
    List<MedicionDepartEntity> getAll();
}
