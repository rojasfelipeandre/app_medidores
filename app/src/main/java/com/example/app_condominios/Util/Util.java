package com.example.app_condominios.Util;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Util {

    private static final String RUTA = "/.APPMEDICION";

    public static boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            return  true;
        }
        return false;
    }

    public static Boolean getAlbumStorageDir(Context context) {
        ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
        File file = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES + RUTA);
        //File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                System.out.println("ALBUM"+ "Album no creado");
                return false;
            }
        }
        return true;
    }

    public static File getNuevaRutaFoto(String idSolicitud, Context context) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        String timeStamp = dateFormat.format(new Date());
        ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
        File mapDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_PICTURES + RUTA);
        File file = new File(mapDirectory + idSolicitud + "_" + timeStamp + ".JPEG");
        return file;
        //return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), RUTA + idSolicitud + "_" + timeStamp + ".JPEG");
    }

    public static String[] getPermisos(Context context, String... permisos) {
        if (context == null) {
            return null;
        }
        List<String> permissionList = new ArrayList<>();
        for (String permission : permisos) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(permission);
            }
        }
        if (!permissionList.isEmpty()) {
            return permissionList.toArray(new String[permissionList.size()]);
        } else {
            return null;
        }
    }

    public static String onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResults) {
        int i = 0;
        String message = "No puede realizar la acción porque no se ha dado el permiso de ";
        boolean entro = false;
        for (String permiso : permissions) {
            switch (permiso) {
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        message += "Almacenamiento";
                        entro = true;
                    }
                    break;
                case Manifest.permission.CAMERA:
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        message += "Camara";
                        entro = true;
                    }
                    break;
                case Manifest.permission.ACCESS_FINE_LOCATION:
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        message += "Ubicación";
                        entro = true;
                    }
                    break;
                default:
            }
            i++;
        }
        if (entro) {
            return message;
        } else {
            return "";
        }
    }

    /*public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }*/
}
