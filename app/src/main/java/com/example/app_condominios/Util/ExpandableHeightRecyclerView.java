package com.example.app_condominios.Util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

public class ExpandableHeightRecyclerView extends RecyclerView {

    boolean expanded = false;

    public ExpandableHeightRecyclerView(Context context)
    {
        super(context);
    }

    public ExpandableHeightRecyclerView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ExpandableHeightRecyclerView(Context context, AttributeSet attrs,
                                        int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public boolean isExpanded()
    {
        return expanded;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        // HACK! TAKE THAT Android!
        if (isExpanded())
        {
            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
                    MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
        }
        else
        {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void setExpanded(boolean expanded)
    {
        this.expanded = expanded;
    }
}