package com.example.app_condominios.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.widget.Spinner;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.example.app_condominios.R;

import java.util.List;
import java.util.function.Consumer;


public class Commons {


    public static ProgressDialog mostrarDialogEspera(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    private static AlertDialog alert;


    @SuppressLint("NewApi")
    public static void mostrarDialogo(Context context, String title, String body, boolean showPositive, String textPositive, boolean showNegative, String textnegative, Consumer<Integer> action, int operacion) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(textPositive, (dialogo, id) -> {
                    if (action != null) {
                        action.accept(operacion);
                    }
                    alert.cancel();
                })
                .setNegativeButton(textnegative, (dialogo, id) -> alert.cancel());
        alert = builder.create();
        alert.show();
        if (!showPositive) alert.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
        if (!showNegative) alert.getButton(DialogInterface.BUTTON_NEGATIVE).setEnabled(false);
    }
}
