package com.example.app_condominios.Util.RedInternet;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class ConexionViewModel extends AndroidViewModel {
    public LiveData<Boolean> connectivity;
    public ConexionViewModel(@NonNull Application application) {
        super(application);
        connectivity = new ConexionRed(application);
    }
}
