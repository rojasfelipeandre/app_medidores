package com.example.app_condominios.Util.RedInternet;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;

public class ConexionRed extends LiveData<Boolean> {
    ConnectivityManager connectivityManager;
    private boolean conexionAnterior;
    private Context context;
    public static final int ERROR_REGISTRO_LOCALIZACION = 99;
    public static final int PROCESO_SIN_INICIAR = 3;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 20000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    final String TAG = "NETWORKLOCAL";

    public ConexionRed(Application application) {
        context = application.getApplicationContext();
        connectivityManager = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActive() {
        notificarUnCambio();
        super.onActive();
        NetworkRequest.Builder builder = new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR);
        connectivityManager.registerNetworkCallback(builder.build(), new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                Log.e("RED", "onAvailable " + network.toString());
                notificarUnCambio();
            }

            @Override
            public void onLost(Network network) {
                Log.e("RED", "onLost " + network.toString());
                notificarUnCambio();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void notificarUnCambio() {
        boolean conexionReciente = estaInternetEncendido();
        if (conexionAnterior != conexionReciente) {
            postValue(conexionReciente);
            conexionAnterior = conexionReciente;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean estaInternetEncendido() {
        if (estaRedEncendida()) {
            return tieneConexionInternet();
        }
        return false;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean estaRedEncendida() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            if (capabilities != null) {

                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    System.out.println(TAG + "CONEXION MOVIL");



                    return true;
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    System.out.println(TAG + "CONEXIÓN WIFI");



                    return true;
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    System.out.println(TAG + "CONEXIÓN DE RED");
                    return true;
                }
            }
        }
        return false;
    }


    public boolean tieneConexionInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}