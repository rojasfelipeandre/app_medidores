package com.example.app_condominios.Util;

import com.example.app_condominios.Models.AñosModel;
import com.example.app_condominios.Models.Response.ResponseEdificios;
import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.Models.Response.ResponseMedicionTipo;
import java.util.ArrayList;

public class LocalMock {

    public static ArrayList<ResponseEdificios> getEdificios(){
        ArrayList<ResponseEdificios> edificios = new ArrayList<>();

        edificios.add(new ResponseEdificios(78, "EDIFICIO A", "http://api.test.corinser.com/public/images/building/78/logo/961220211007042327.jpg","Miraflores"));
        edificios.add(new ResponseEdificios(79, "EDIFICIO B", "http://api.test.corinser.com/public/images/building/79/logo/384120211007042431.jpg","San Isidro"));
        edificios.add(new ResponseEdificios(80, "EDIFICIO C", "http://api.test.corinser.com/public/images/building/80/logo/230620211025092049.jpg","San Borja"));
        edificios.add(new ResponseEdificios(81, "EDIFICIO D", "http://api.test.corinser.com/public/images/building/81/logo/900320211025095448.jpg","San Luis"));
        edificios.add(new ResponseEdificios(81, "EDIFICIO F", "http://api.test.corinser.com/public/images/building/82/logo/379820211025095604.jpg","La Molina"));
        edificios.add(new ResponseEdificios(81, "EDIFICIO G", "http://api.test.corinser.com/public/images/building/83/logo/146220211025095657.jpg","Villa María"));
        edificios.add(new ResponseEdificios(84, "EDIFICIO H", "http://api.test.corinser.com/public/images/building/84/logo/113720211025101442.jpg","Los Olivos"));
        edificios.add(new ResponseEdificios(85, "EDIFICIO I", "http://api.test.corinser.com/public/images/building/85/logo/977020211025101535.png","Jesús María"));
        edificios.add(new ResponseEdificios(86, "EDIFICIO J", "http://api.test.corinser.com/public/images/building/86/logo/787420211025101622.jpg","Pueblo Libre"));
        edificios.add(new ResponseEdificios(87, "EDIFICIO K", "http://api.test.corinser.com/public/images/building/87/logo/434320211025101703.png","Barranco"));
        return edificios;
    }

    public static ArrayList<ResponseMedicionTipo> getTipoMedicion(){
        ArrayList<ResponseMedicionTipo> medicionTipo = new ArrayList<>();

        medicionTipo.add(new ResponseMedicionTipo(1, "01","Agua",""));
        medicionTipo.add(new ResponseMedicionTipo(2, "02","Luz",""));
        medicionTipo.add(new ResponseMedicionTipo(3, "03","Medicion de Agua",""));
        medicionTipo.add(new ResponseMedicionTipo(4, "04","Medicion de Luz",""));
        medicionTipo.add(new ResponseMedicionTipo(5, "05","Gas",""));
        medicionTipo.add(new ResponseMedicionTipo(6, "06","Agua helada",""));
        medicionTipo.add(new ResponseMedicionTipo(7, "07","Agua caliente",""));
        return medicionTipo;
    }

    public static ArrayList<ResponseMedicionDepart> getMedicondepa(){
        ArrayList<ResponseMedicionDepart> medicionDepa = new ArrayList<>();

        medicionDepa.add(new ResponseMedicionDepart(551, "OF101","0.000","10.000","http://api.test.corinser.com/public/images/building/79/measurement/01/10-2021/52220211007053430.jpg", 1));
        medicionDepa.add(new ResponseMedicionDepart(552, "OF102","0.000","10.000","http://api.test.corinser.com/public/images/building/79/measurement/01/10-2021/414920211025115550.jpg",1));
        medicionDepa.add(new ResponseMedicionDepart(553, "OF103","0.000","16.000","http://api.test.corinser.com/public/images/building/79/measurement/01/10-2021/52220211007053430.jpg",1));
        medicionDepa.add(new ResponseMedicionDepart(554, "OF104","0.000","18.000","",1));
        medicionDepa.add(new ResponseMedicionDepart(555, "OF105","0.000","10.000","http://api.test.corinser.com/public/images/building/79/measurement/01/10-2021/52220211007053430.jpg",1));
        medicionDepa.add(new ResponseMedicionDepart(556, "OF106","0.000","18.000","http://api.test.corinser.com/public/images/building/79/measurement/01/10-2021/414920211025115550.jpg",1));
        medicionDepa.add(new ResponseMedicionDepart(557, "OF107","0.000","16.000","",1));
        medicionDepa.add(new ResponseMedicionDepart(558, "OF108","0.000","18.000","http://api.test.corinser.com/public/images/building/79/measurement/01/10-2021/414920211025115550.jpg",1));
        return medicionDepa;
    }

    public static ArrayList<AñosModel> getAños(){
        ArrayList<AñosModel> añosmodel = new ArrayList<>();

        añosmodel.add(new AñosModel("2020"));
        añosmodel.add(new AñosModel("2021"));
        añosmodel.add(new AñosModel("2022"));
        añosmodel.add(new AñosModel("2023"));
        añosmodel.add(new AñosModel("2024"));
        añosmodel.add(new AñosModel("2025"));
        añosmodel.add(new AñosModel("2026"));
        añosmodel.add(new AñosModel("2027"));
        añosmodel.add(new AñosModel("2028"));
        añosmodel.add(new AñosModel("2029"));
        añosmodel.add(new AñosModel("2039"));
       return añosmodel;
    }
}
