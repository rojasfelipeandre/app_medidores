package com.example.app_condominios.Util.RedInternet;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.EdificiosDao;
import com.example.app_condominios.Database.dao.MedicionDepartDao;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.dao.TipoMedicionDao;

public class ServicioDesconectado extends ConexionServicio{

    EdificiosDao edificiosDao;
    TipoMedicionDao tipoMedicionDao;
    MedicionDepartDao medicionDepartDao;
    RegistroDao registroDao;


    public ServicioDesconectado(Context context) {
        super(context);
        AppDatabase database = AppDatabase.getDatabaseInstance(context);
        edificiosDao = database.edificiosDao();
        tipoMedicionDao = database.tipoMedicionDao();
        medicionDepartDao = database.medicionDepartDao();
        registroDao = database.registroDao();
    }


    public static boolean existenRegistrosSinConexion(Context context){
        AppDatabase database = AppDatabase.getDatabaseInstance(context);
        RegistroDao registroDao= database.registroDao();
        return registroDao.getAll().size() > 0;
    }

    /*@Override
    public void sincronizarProcesos() {
        mostrarSolicitudDeConexionInternet();
    }*/

    /*public void mostrarSolicitudDeConexionInternet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Es necesario estar conectado a una Red.")
                .setTitle("SOLICITUD")
                .setCancelable(false)
                .setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        alert.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);
    }*/
}