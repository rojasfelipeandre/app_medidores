package com.example.app_condominios.Service;

import com.example.app_condominios.Models.Request.RequestPadreResponseMedicionTipo;
import com.example.app_condominios.Models.Request.RequestUpdateRegistro;
import com.example.app_condominios.Models.Response.ResponseEdificios;
import com.example.app_condominios.Models.Response.ResponseLogin;
import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.Models.Response.ResponseRegistroMedicion;
import com.example.app_condominios.Models.Response.ResponseRegistroRecuperar;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiServices {


    //Iniciar Sesion
    @POST("auth/login")
    Call<ResponseLogin> obtenerLogin(@Query("email") String email,
                                     @Query("password") String password);

    //Recuperar contraseña
    @POST("auth/password_email")
    Call<ResponseRegistroRecuperar> obtenerRecuperarContra(@Body RequestBody body);

    //Obtener Array de edificios
    @GET("buildings")
    Call<ArrayList<ResponseEdificios>> obtenerEdificios(@Header("Authorization") String token);

    //Obtener Array de tipos de medicion
    @GET("measurements-type")
    Call<RequestPadreResponseMedicionTipo> obtenerMedicion(@Header("Authorization") String token);

    //Obtener Array de medicion por departamento
    @POST("measurements")
    Call<ArrayList<ResponseMedicionDepart>> obtenerMedicionDepa(@Header("Authorization") String token, @Body RequestBody body);

    //Actualizar medicion por departamento
    @PUT("measurements/{id}")
    Call<ResponseRegistroMedicion> actualizarMedicion(@Header("Authorization") String token,
                                                                 @Path("id") int id,
                                                                 @Body RequestUpdateRegistro requestUpdateRegistro);


}
