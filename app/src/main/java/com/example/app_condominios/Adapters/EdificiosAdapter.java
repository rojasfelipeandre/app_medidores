package com.example.app_condominios.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app_condominios.Models.Response.ResponseEdificios;
import com.example.app_condominios.R;
import com.example.app_condominios.Util.ShapeImage;

import java.util.ArrayList;

public class EdificiosAdapter extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<ResponseEdificios> item;

    public EdificiosAdapter(Activity activity, ArrayList<ResponseEdificios> item){
        this.activity=activity;
        this.item=item;
    }

    @Override
    public int getCount(){
        return item.size();
    }

    public void clear(){
        item.clear();
    }

    public void addAdll(ArrayList<ResponseEdificios>item){
        for(int i=0; i < item.size(); i++){
            item.add(item.get(i));
        }
    }

    @Override
    public Object getItem(int arg0){
        return item.get(arg0);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_listcondominios, null);
        }
        ResponseEdificios edificios = item.get(position);

        TextView description = (TextView) v.findViewById(R.id.item_title);
        ShapeImage imagen = (ShapeImage) v.findViewById(R.id.item_img);
        //imagen.setClipToOutline(true);
        description.setText(edificios.getName_building());
        if(edificios.getImage_building() != null){
            Glide.with(imagen)
                    .load(edificios.getImage_building())
                    .centerCrop()
                    .into(imagen);
        }else{
            Glide.with(imagen)
                    .load(R.mipmap.preview)
                    .into(imagen);
        }

        //imagen.setImageDrawable(edificios.getImage_building());

        return v;
    }
}
