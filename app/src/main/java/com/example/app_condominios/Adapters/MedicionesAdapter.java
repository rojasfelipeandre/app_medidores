package com.example.app_condominios.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app_condominios.Models.Response.ResponseMedicionTipo;
import com.example.app_condominios.R;

import java.util.ArrayList;

public class MedicionesAdapter extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<ResponseMedicionTipo> item;

    public MedicionesAdapter(Activity activity, ArrayList<ResponseMedicionTipo> item){
        this.activity=activity;
        this.item=item;
    }

    @Override
    public int getCount(){
        return item.size();
    }

    public void clear(){
        item.clear();
    }

    public void addAdll(ArrayList<ResponseMedicionTipo>item){
        for(int i=0; i < item.size(); i++){
            item.add(item.get(i));
        }
    }

    @Override
    public Object getItem(int arg0){
        return item.get(arg0);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_listmediciones, null);
        }
        ResponseMedicionTipo edificios = item.get(position);

        TextView description = (TextView) v.findViewById(R.id.item_title);
        ImageView imagen = (ImageView) v.findViewById(R.id.item_img);

        description.setText(edificios.getName_measurement_type());

        if(edificios.getImage_measurement() != null){
            Glide.with(imagen)
                    .load(edificios.getImage_measurement())
                    .placeholder(R.mipmap.preview)
                    .into(imagen);
        }else{
            Glide.with(imagen)
                    .load(R.mipmap.preview)
                    .into(imagen);
        }
        return v;
    }
}
