package com.example.app_condominios.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.R;
import com.example.app_condominios.Util.SimpleRow;
import com.example.app_condominios.Util.SimpleRowEditar;

import java.util.List;

public class MedicionDepaAdapter extends RecyclerView.Adapter<MedicionDepaAdapter.EventosUViewHolder> {

    List<ResponseMedicionDepart> listaDepa;
    private SimpleRow eventos;
    private int idmedicion;
    //AppDatabase database;
    private SimpleRowEditar eventoseditar;
    Context context;

    public MedicionDepaAdapter(List<ResponseMedicionDepart> listaDepa) {
        this.listaDepa = listaDepa;
    }

    @NonNull
    @Override
    public EventosUViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listdepartamentos, parent, false);
        return new EventosUViewHolder(view, eventos,eventoseditar);
    }

    @Override
    public void onBindViewHolder(@NonNull EventosUViewHolder holder, int position) {
        holder.bind(listaDepa.get(position));
    }

    @Override
    public int getItemCount() {
        return listaDepa.size();
    }

    public void setEventos(SimpleRow eventos) {
        this.eventos = eventos;
    }

    public void setEventoseditar(SimpleRowEditar eventoseditar) { this.eventoseditar = eventoseditar; }

    public void setIdMedicion(int idmedicion){ this.idmedicion = idmedicion; }

    public void setData(List<ResponseMedicionDepart> listaEventosu) {
        this.listaDepa.clear();
        this.listaDepa.addAll(listaEventosu);
        notifyDataSetChanged();
    }

    class EventosUViewHolder extends RecyclerView.ViewHolder {

        private ImageView image = itemView.findViewById(R.id.image);
        private TextView tvnrdpt = itemView.findViewById(R.id.tvnrdpt);
        private TextView tvcodigo = itemView.findViewById(R.id.tvcodigo);
        private TextView tvlecactual = itemView.findViewById(R.id.tvlecactual);
        private TextView tvlecpasada = itemView.findViewById(R.id.tvlecpasada);
        private TextView tvlecturaItem = itemView.findViewById(R.id.tvlecturaItem);
        private TextView itemlecturapas = itemView.findViewById(R.id.itemlecturapas);
        private LinearLayout editar = itemView.findViewById(R.id.editar);
        private LinearLayout layt = itemView.findViewById(R.id.layt);
        private LinearLayout refresh = itemView.findViewById(R.id.refresh);

        private View container = itemView.findViewById(R.id.container);


        EventosUViewHolder(@NonNull View itemView, final SimpleRow onAcciones, final  SimpleRowEditar onEditar) {
            super(itemView);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    onAcciones.onClic(position);
                }
            });

            editar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    onEditar.onClic(position);
                }
            });

        }

        void bind(ResponseMedicionDepart listaDepa) {
            switch (listaDepa.getIs_closed()){
                // = = editar pero si la medicion actual viene en 00.000 que permita registrar mas no editar
                case 0:
                    if(listaDepa.getReading_month_now() == null){
                        layt.setBackgroundResource(R.drawable.shape_cardview_bg);
                        tvnrdpt.setText(listaDepa.getNumber_estate());
                        tvcodigo.setText(String.valueOf(listaDepa.getMeasurement_id()));
                        switch (idmedicion){
                            //Agua
                            case 1:
                                tvlecactual.setText(Html.fromHtml("0" + " " + "m<sup><<small>3</small></sup>"));
                                if(listaDepa.getReading_month_past() != null){
                                    tvlecpasada.setText(Html.fromHtml( listaDepa.getReading_month_past()+ " " + "m<sup><small>3</small></sup>"));
                                }else{
                                    tvlecpasada.setText(Html.fromHtml("0" + " " + "m<sup><small>3</small></sup>"));
                                }

                                break;

                            //Luz
                            case 2:
                                tvlecactual.setText("0" + " " + "kWh");
                                if(listaDepa.getReading_month_past() != null){
                                    tvlecpasada.setText(listaDepa.getReading_month_past() + " " + "kWh");
                                }else{
                                    tvlecpasada.setText("0" + " " + "kWh");
                                }
                                break;

                            default:
                                tvlecactual.setText(Html.fromHtml("0" + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml("0" + " " + "m<sup><<small>3</small></sup>"));
                                break;
                        }
                        if(listaDepa.getImage_attached() != null){
                            Glide.with(image)
                                    .load(listaDepa.getImage_attached())
                                    .placeholder(R.mipmap.preview)
                                    .into(image);
                        }else{
                            Glide.with(image)
                                    .load(R.mipmap.preview)
                                    .into(image);
                        }
                    }else if (listaDepa.getReading_month_now().equals("0.000")) {
                        layt.setBackgroundResource(R.drawable.shape_cardview_bg);
                        tvnrdpt.setText(listaDepa.getNumber_estate());
                        tvcodigo.setText(String.valueOf(listaDepa.getMeasurement_id()));
                        switch (idmedicion){
                            //Agua
                            case 1:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;

                            //Luz
                            case 2:
                                tvlecactual.setText(listaDepa.getReading_month_now() + " " + "kWh");
                                tvlecpasada.setText(listaDepa.getReading_month_past() + " " + "kWh");
                                break;

                            default:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;
                        }
                        if(listaDepa.getImage_attached() != null){
                            Glide.with(image)
                                    .load(listaDepa.getImage_attached())
                                    .placeholder(R.mipmap.preview)
                                    .into(image);
                        }else{
                            Glide.with(image)
                                    .load(R.mipmap.preview)
                                    .into(image);
                        }

                    }else if(listaDepa.getReading_month_now().equals("00.000")){
                        layt.setBackgroundResource(R.drawable.shape_cardview_bg);
                        tvnrdpt.setText(listaDepa.getNumber_estate());
                        tvcodigo.setText(String.valueOf(listaDepa.getMeasurement_id()));
                        switch (idmedicion){
                            //Agua
                            case 1:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;

                            //Luz
                            case 2:
                                tvlecactual.setText(listaDepa.getReading_month_now() + " " + "kWh");
                                tvlecpasada.setText(listaDepa.getReading_month_past() + " " + "kWh");
                                break;

                            default:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;
                        }
                        if(listaDepa.getImage_attached() != null){
                            Glide.with(image)
                                    .load(listaDepa.getImage_attached())
                                    .placeholder(R.mipmap.preview)
                                    .into(image);
                        }else{
                            Glide.with(image)
                                    .load(R.mipmap.preview)
                                    .into(image);
                        }
                    }else{
                        container.setEnabled(false);
                        editar.setVisibility(View.VISIBLE);
                        tvnrdpt.setTextColor(Color.parseColor("#FFFFFF"));
                        tvcodigo.setTextColor(Color.parseColor("#FFFFFF"));
                        tvlecactual.setTextColor(Color.parseColor("#FFFFFF"));
                        tvlecpasada.setTextColor(Color.parseColor("#FFFFFF"));
                        tvlecturaItem.setTextColor(Color.parseColor("#FFFFFF"));
                        itemlecturapas.setTextColor(Color.parseColor("#FFFFFF"));
                        layt.setBackgroundResource(R.drawable.shape_cardview_bg2);

                        tvnrdpt.setText(listaDepa.getNumber_estate());
                        tvcodigo.setText(String.valueOf(listaDepa.getMeasurement_id()));
                        switch (idmedicion){
                            //Agua
                            case 1:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;

                            //Luz
                            case 2:
                                tvlecactual.setText(listaDepa.getReading_month_now() + " " + "kWh");
                                tvlecpasada.setText(listaDepa.getReading_month_past() + " " + "kWh");
                                break;

                            default:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;
                        }
                        if(listaDepa.getImage_attached() != null){
                            Glide.with(image)
                                    .load(listaDepa.getImage_attached())
                                    .placeholder(R.mipmap.preview)
                                    .into(image);
                        }else{
                            Glide.with(image)
                                    .load(R.mipmap.preview)
                                    .into(image);
                        }
                    }
                    break;

                //no editar
                case 1:
                    container.setEnabled(false);
                    layt.setBackgroundResource(R.drawable.shape_cardview_bg_grey);
                    tvnrdpt.setText(listaDepa.getNumber_estate());
                    tvcodigo.setText(String.valueOf(listaDepa.getMeasurement_id()));
                    switch (idmedicion){
                        //Agua
                        case 1:
                            tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                            tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                            break;

                        //Luz
                        case 2:
                            tvlecactual.setText(listaDepa.getReading_month_now() + " " + "kWh");
                            tvlecpasada.setText(listaDepa.getReading_month_past() + " " + "kWh");
                            break;

                        default:
                            tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                            tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                            break;
                    }
                    if(listaDepa.getImage_attached() != null){
                        Glide.with(image)
                                .load(listaDepa.getImage_attached())
                                .placeholder(R.mipmap.preview)
                                .into(image);
                    }else{
                        Glide.with(image)
                                .load(R.mipmap.preview)
                                .into(image);
                    }

                break;

                //espera de subida
                case 2:
                    tvnrdpt.setText(listaDepa.getNumber_estate());
                    tvcodigo.setText(String.valueOf(listaDepa.getMeasurement_id()));
                    /*List<RegistroEntity> reg = database.registroDao().getAll();
                    for(RegistroEntity registro: reg){
                    }
                    }*/
                    switch (idmedicion){

                            //Agua
                            case 1:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now() + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;

                            //Luz
                            case 2:
                                tvlecactual.setText(listaDepa.getReading_month_now()  + " " + "kWh");
                                tvlecpasada.setText(listaDepa.getReading_month_past() + " " + "kWh");
                                break;

                            default:
                                tvlecactual.setText(Html.fromHtml(listaDepa.getReading_month_now()  + " " + "m<sup><<small>3</small></sup>"));
                                tvlecpasada.setText(Html.fromHtml(listaDepa.getReading_month_past() + " " + "m<sup><small>3</small></sup>"));
                                break;
                        }

                    container.setEnabled(false);
                    refresh.setVisibility(View.VISIBLE);

                break;
            }
        }

    }


}
