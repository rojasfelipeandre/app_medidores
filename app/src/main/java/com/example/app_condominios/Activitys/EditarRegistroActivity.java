package com.example.app_condominios.Activitys;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Models.Request.RequestUpdateRegistro;
import com.example.app_condominios.Models.Response.ResponseRegistroMedicion;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.MostrarFotoTask;
import com.example.app_condominios.Util.RedInternet.ConexionServicio;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;
import com.example.app_condominios.Util.Util;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.app_condominios.Util.Constants.compressImage;

public class EditarRegistroActivity extends AppCompatActivity {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private SharedPreferences prefs;
    private ImageView back,view;
    private LinearLayout tomarfoto, editarfoto, guardar2;
    private EditText registro;
    private TextView numerdepa,textmesano;
    private TextInputLayout tireg;
    private String id, nombre, mesuranmentid;
    private ProgressDialog pDialog;
    private String tmpRuta, token,iddepa,medicionid,nombremedicion,phatImage,numdepa, mesnum,phatimagenurl;
    private Bitmap bitmap;
    AppDatabase database;
    RegistroDao registroDao;
    private ConexionServicio conexionServicio;
    String medi,fot,imagen,med, enviarbase64,mesano,añoenviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_registro);

        changeStatusBarColor();
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        token = prefs.getString(Constants.token,"");
        database = AppDatabase.getDatabaseInstance(EditarRegistroActivity.this);
        id = getIntent().getStringExtra("id_edificio");
        numdepa = getIntent().getStringExtra("numerodepa");
        nombre = getIntent().getStringExtra("nombre_edificio");
        iddepa = getIntent().getStringExtra("iddepartamento");
        medicionid = getIntent().getStringExtra("id_medicion");
        nombremedicion = getIntent().getStringExtra("nombre_medicion");
        imagen =  getIntent().getStringExtra("image");
        med =  getIntent().getStringExtra("medi");
        mesano = getIntent().getStringExtra("mesano");
        mesnum = getIntent().getStringExtra("mesnum");
        añoenviar = getIntent().getStringExtra("anore");
        System.out.println("idedificio: " + id + " iddepa: " + iddepa);
        back = findViewById(R.id.atras);
        view = findViewById(R.id.imvfoto);
        numerdepa = findViewById(R.id.textnumerodepa);
        textmesano = findViewById(R.id.textmesano);
        tomarfoto = findViewById(R.id.idtomarfoto);
        editarfoto = findViewById(R.id.ideditarfoto);
        guardar2 = findViewById(R.id.btnguardar2);
        registro = findViewById(R.id.edtregistro);
        //nomimg = findViewById(R.id.txtidimagen);
        tireg = findViewById(R.id.tireg);

        if(imagen != null){
            phatimagenurl = (convertUrlToBase64(imagen));
            if(!phatimagenurl.isEmpty()){
                System.out.println("Imagen supuestamente llena");
                view.setVisibility(View.VISIBLE);
                Glide.with(view)
                        .load(imagen)
                        .into(view);

                guardar2.setVisibility(View.VISIBLE);
                System.out.println("path: " + phatimagenurl);
            }
        }else{
            System.out.println("Imagen vacia");
        }

        if(med != null){
            registro.setText(med);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarRegistroActivity.this, MedicionDepartamentoActivity.class);
                intent.putExtra("id_edificio", id);
                intent.putExtra("nombre_edificio", nombre);
                intent.putExtra("id_medicion", medicionid);
                intent.putExtra("nombre_medicion", nombremedicion);
                intent.putExtra("mesnum", mesnum);
                intent.putExtra("mes", mesano);
                intent.putExtra("anore",añoenviar);
                startActivity(intent);
                finish();
            }
        });

        numerdepa.setText("Dpto "+numdepa +" - "+ nombre);
        textmesano.setText(mesano);

        registro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(registro.getText().toString().isEmpty()){
                    guardar2.setVisibility(View.VISIBLE);
                }else{
                    if(tmpRuta == null){
                        guardar2.setVisibility(View.VISIBLE);
                    }else{
                        tireg.setError(null);
                        guardar2.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(registro.getText().toString().isEmpty()){
                    guardar2.setVisibility(View.VISIBLE);
                }else{
                    if(tmpRuta == null){
                        guardar2.setVisibility(View.VISIBLE);
                    }else{
                        guardar2.setVisibility(View.VISIBLE);
                    }

                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(registro.getText().toString().isEmpty()){
                    guardar2.setVisibility(View.VISIBLE);
                }else{
                    if(tmpRuta == null){
                        guardar2.setVisibility(View.VISIBLE);
                    }else{
                        guardar2.setVisibility(View.VISIBLE);

                    }

                }
            }
        });

        tomarfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarfoto();
            }
        });

        editarfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarfoto();
            }
        });

        guardar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar();
            }
        });

        DisplayProgressDialog();
    }

    public String convertUrlToBase64(String url) {
        URL newurl;
        Bitmap bitmap;
        String base64 = "";
            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                newurl = new URL(url);
                bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return base64;
    }

    private void validar() {
        if(phatImage != null && phatImage != ""){
            System.out.println("Enviar pahtimage");
            enviarbase64 = "data:image/jpeg;base64,/"+phatImage;
        }else{
            System.out.println("Enviar phatimagenurl");
            if(phatimagenurl.isEmpty()){
                enviarbase64="";
            }else{
                enviarbase64 = "data:image/jpeg;base64,/"+phatimagenurl;
            }
        }

        final String compruebadecimal = registro.getEditableText().toString().trim();
        final String myregexpInt = "-?\\d+";
        final String myregexpDou="^\\d*\\.\\d+|\\d+\\.\\d*$";
        if(registro.getText().toString().isEmpty()){
            tireg.setError("Ingresar valor del contómetro");
        }else if(!compruebadecimal.matches(myregexpDou)) {
            tireg.setError("Ingrese un valor decimal valido");
        }else if(enviarbase64.isEmpty()){
            Toast.makeText(EditarRegistroActivity.this, "Envie una foto", Toast.LENGTH_SHORT).show();
        }else{
            tireg.setError(null);
            alertDialog();
        }
    }

    private void alertDialog() {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(EditarRegistroActivity.this);
        builder.setCancelable(false);
        builder.setTitle("Editar Medición")
                .setMessage("¿Seguro que deseas editar la medición?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        guardarRegistro();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create();
        builder.show();
    }

    private void guardarRegistro() {
        if(phatImage != null && phatImage != ""){
            System.out.println("Enviar pahtimage");
            enviarbase64 = ("data:image/jpeg;base64,"+phatImage);
        }else{
            System.out.println("Enviar phatimagenurl");
            enviarbase64 = ("data:image/jpeg;base64,"+phatimagenurl);
        }

        int measurementid = Integer.parseInt(iddepa);
        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
        requestUpdateRegistro.setReading_month_now(Double.parseDouble(registro.getText().toString()));
        requestUpdateRegistro.setImage_attached(enviarbase64);
        requestUpdateRegistro.setToken(token);
        System.out.println("Imagen que se esta enviando:  "+ enviarbase64);

        Gson gson = new Gson();
        String JSON = gson.toJson(requestUpdateRegistro);
        Log.v("JsonAcom: ",JSON);


        if(NetworkUtils.tieneAccesoInternet(EditarRegistroActivity.this)){
            System.out.println("Registro con internet");
            Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid,requestUpdateRegistro);
            callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                @Override
                public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    System.out.println("responseResumen: "+response);
                    switch (response.code()){
                        case 200:
                            System.out.println(response.body().getMessage());
                            tmpRuta = "";
                            Intent intent = new Intent(EditarRegistroActivity.this, ConfirmacionRegistroActivity.class);
                            intent.putExtra("id_medicion", medicionid);
                            intent.putExtra("nombre_medicion", nombremedicion);
                            intent.putExtra("id_edificio", id);
                            intent.putExtra("nombre_edificio", nombre);
                            intent.putExtra("measurementid", String.valueOf(measurementid));
                            intent.putExtra("mesnum", mesnum);
                            intent.putExtra("mes", mesano);
                            intent.putExtra("anore",añoenviar);
                            startActivity(intent);
                            finish();
                        break;

                        case 422:
                            Toast.makeText(EditarRegistroActivity.this,getString(R.string.error_inesperado) , Toast.LENGTH_SHORT).show();
                            break;
                        case 400:
                            SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent2 = new Intent(EditarRegistroActivity.this, LoginActivity.class);
                            startActivity(intent2);
                            finish();
                            break;
                        default:
                            Toast.makeText(EditarRegistroActivity.this,getString(R.string.error_inesperado) , Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                    Toast.makeText(EditarRegistroActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                }
            });

        }else{
            System.out.println("Registro sin internet");
            registroDao = database.registroDao();

            RegistroEntity entity = new RegistroEntity();
            entity.setReading_month_now(Double.parseDouble(registro.getText().toString()));
            entity.setImage_attached(enviarbase64);
            System.out.println("FotoOffline: "+enviarbase64);
            entity.setMeasurementid(measurementid);
            entity.setEstado(1); //en espera de registro a servicio
            registroDao.insert(entity);

            tmpRuta = "";
            Intent intent = new Intent(EditarRegistroActivity.this, ConfirmacionRegistroActivity.class);
            intent.putExtra("id_medicion", medicionid);
            intent.putExtra("nombre_medicion", nombremedicion);
            intent.putExtra("id_edificio", id);
            intent.putExtra("nombre_edificio", nombre);
            intent.putExtra("measurementid", String.valueOf(measurementid));
            intent.putExtra("mesnum", mesnum);
            intent.putExtra("mes", mesano);
            intent.putExtra("anore",añoenviar);
            startActivity(intent);
            finish();

        }

    }

    private void tomarfoto() {
        if (ActivityCompat.checkSelfPermission(EditarRegistroActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(EditarRegistroActivity.this, "No puede realizar la acción porque no se ha dado el permiso de Almacenamiento.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(EditarRegistroActivity.this)) {
                File file = Util.getNuevaRutaFoto("Con", EditarRegistroActivity.this);
                tmpRuta = file.getPath();
                System.out.println("file: " + file);
                System.out.println("tmpRuta: " + tmpRuta);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(EditarRegistroActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
                System.out.println("outputUri: " + outputUri);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "RegistroTask", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, 1);
            } else {
                Toast.makeText(EditarRegistroActivity.this, getResources().getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(EditarRegistroActivity.this, getResources().getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
        }
    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(EditarRegistroActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EditarRegistroActivity.this, MedicionDepartamentoActivity.class);
        intent.putExtra("id_edificio", id);
        intent.putExtra("nombre_edificio", nombre);
        intent.putExtra("id_medicion", medicionid);
        intent.putExtra("nombre_medicion", nombremedicion);
        intent.putExtra("mesnum", mesnum);
        intent.putExtra("mes", mesano);
        intent.putExtra("anore",añoenviar);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            String Pa1 = (compressImage(EditarRegistroActivity.this,tmpRuta));
            phatImage = (CargarFoto(Pa1));
            if (phatImage == null || phatImage == "") {
                System.out.println("phatImagenNul: "+phatImage);
            } else {
                view.setVisibility(View.VISIBLE);
                tomarfoto.setVisibility(View.GONE);
                editarfoto.setVisibility(View.VISIBLE);

                loadBitmap(tmpRuta, view);
                String Pa2 = (compressImage(EditarRegistroActivity.this,tmpRuta));
                phatImage = (CargarFoto(Pa2));

                System.out.println("phatImageNoNul: " + phatImage);
                /*if (tmpRuta == null) {
                    System.out.println("Vacio");
                } else {
                    String separador = Pattern.quote("/");
                    String[] parts = tmpRuta.split(separador);
                    String part10 = parts[9];
                    System.out.println("Nme: " + part10);

                    String separador2 = Pattern.quote("_");
                    String[] parts2 = part10.split(separador2);
                    String uno = parts2[0];
                    String dos = parts2[1];
                    System.out.println("Nme2: " + dos);
                    nomimg.setText("Foto adjunta: " + dos);
                }*/
            }
        }

    }

    public String CargarFoto (String path_foto){
        String foto_str="";
        String encodedString="";
        if(path_foto!=null ){
            try {
                File file = new File(path_foto);
                try (FileInputStream fis = new FileInputStream(file)) {
                    byte[] bytes = new byte[(int) file.length()];
                    fis.read(bytes);
                    encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                    //encodedString = Base64.encodeToString(Constants.resizeImage(EditarRegistroActivity.this,bytes,20,20), Base64.DEFAULT);
                } catch (IOException ex) {
                    encodedString = null;
                    System.out.println("Error al convertir la imagen. " + ex.getMessage() +  ex);
                }

                //foto_str = Base64.encodeToString(String.valueOf(bitmapFile),Base64.DEFAULT);
            }catch (Exception e){
                e.printStackTrace();
                encodedString = null;
                return encodedString;
            }
            Log.e("foto",foto_str);
        }
        System.out.println("encodedString: " + encodedString);
        return encodedString;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    Toast.makeText(EditarRegistroActivity.this, "Error al cargar la foto", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

}