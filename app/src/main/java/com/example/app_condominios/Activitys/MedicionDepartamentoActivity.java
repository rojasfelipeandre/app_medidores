package com.example.app_condominios.Activitys;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_condominios.Adapters.MedicionDepaAdapter;
import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.MedicionDepartDao;
import com.example.app_condominios.Database.dao.MedicionDepartDao2;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Models.AñosModel;
import com.example.app_condominios.Models.Request.RequestUpdateRegistro;
import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.Models.Response.ResponseRegistroMedicion;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.ExpandableHeightRecyclerView;
import com.example.app_condominios.Util.LocalMock;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;
import com.example.app_condominios.Util.SimpleRow;
import com.example.app_condominios.Util.SimpleRowEditar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicionDepartamentoActivity extends AppCompatActivity {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private SharedPreferences prefs;
    private CardView calen, calendarcolor, medi, hom, en, feb, mar, abr, may, jun, jul, agos, sep, oct, nov, dic;
    private LinearLayout lyen, lyfeb, lymar, lyabr, lymay, lyjun, lyjul, lyagos, lysep, lyoct, lynov, lydic;
    private TextView tven, tvfeb, tvmar, tvabr, tvmay, tvjun, tvjul, tvagos, tvsep, tvoct, tvnov, tvdic;
    private TextView textTitulo, textdireccion;
    private LinearLayout contentcalendar, sinregistro;
    private ImageView atras;
    private EditText edtaño;
    private ExpandableHeightRecyclerView recydepartamentos;
    private MedicionDepaAdapter adapter;
    private String id, nombre, medicionid, nombremedicion, token, param1, param2, param3, an, mesenviar, mesnum = "0", mesnumori, mesori, mesanooo;
    String imagen;
    double medicion;
    private int i, idtab, idestado, iddepa, medid ,añoact, añopast;
    private ProgressDialog pDialog;
    private ProgressDialog pDialogg;
    private List<ResponseMedicionDepart> listmeiciondepa = new ArrayList<>();
    private String añoenviar, añorecibir,añoutilizar, mesutilizar;
    private Spinner spin;
    AppDatabase database;
    MedicionDepartDao medicionDepartDao;
    MedicionDepartDao2 medicionDepartDao2;
    RegistroDao registroDao;
    private int idofline,total,pendientes1,enviados1;
    private ArrayList<ResponseMedicionDepart> listmeiciondepaOffline = new ArrayList<>();
    private List<AñosModel> listaactividadd = new ArrayList();
    private int añ;
    private FloatingActionButton fab;
    private UploadOfflineAsincrona uploadOfflineAsincrona;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_medicion);
        changeStatusBarColor();
        database = AppDatabase.getDatabaseInstance(MedicionDepartamentoActivity.this);
        registroDao = database.registroDao();
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        token = prefs.getString(Constants.token, "");
        calen = findViewById(R.id.cvCalendar);
        calendarcolor = findViewById(R.id.cvCalendarcolor);
        medi = findViewById(R.id.cvmedicion);
        hom = findViewById(R.id.cvhome);
        recydepartamentos = findViewById(R.id.recy_departamentos);
        recydepartamentos.setExpanded(true);
        sinregistro = findViewById(R.id.sinregistro);
        spin = findViewById(R.id.spin);

        //Cardview
        en = findViewById(R.id.cvenero);
        feb = findViewById(R.id.cvfebrero);
        mar = findViewById(R.id.cvmarzo);
        abr = findViewById(R.id.cvabril);
        may = findViewById(R.id.cvmayo);
        jun = findViewById(R.id.cvjunio);
        jul = findViewById(R.id.cvjulio);
        agos = findViewById(R.id.cvagosto);
        sep = findViewById(R.id.cvseptiembre);
        oct = findViewById(R.id.cvoctubre);
        nov = findViewById(R.id.cvnoviembre);
        dic = findViewById(R.id.cvdiciembre);

        //layout
        lyen = findViewById(R.id.lyenero);
        lyfeb = findViewById(R.id.lyfebrero);
        lymar = findViewById(R.id.lymarzo);
        lyabr = findViewById(R.id.lyabril);
        lymay = findViewById(R.id.lymayo);
        lyjun = findViewById(R.id.lyjunio);
        lyjul = findViewById(R.id.lyjulio);
        lyagos = findViewById(R.id.lyagosto);
        lysep = findViewById(R.id.lyseptiembre);
        lyoct = findViewById(R.id.lyoctubre);
        lynov = findViewById(R.id.lynoviembre);
        lydic = findViewById(R.id.lydiciembre);

        //Texview
        tven = findViewById(R.id.tvenero);
        tvfeb = findViewById(R.id.tvfebrero);
        tvmar = findViewById(R.id.tvmarzo);
        tvabr = findViewById(R.id.tvabril);
        tvmay = findViewById(R.id.tvmayo);
        tvjun = findViewById(R.id.tvjunio);
        tvjul = findViewById(R.id.tvjulio);
        tvagos = findViewById(R.id.tvagosto);
        tvsep = findViewById(R.id.tvseptiembre);
        tvoct = findViewById(R.id.tvOctubre);
        tvnov = findViewById(R.id.tvnoviembre);
        tvdic = findViewById(R.id.tvdiciembre);
        edtaño = findViewById(R.id.edtaño);

        textTitulo = findViewById(R.id.textTitulo);
        textdireccion = findViewById(R.id.textdireccion);
        contentcalendar = findViewById(R.id.contentcalendar);
        atras = findViewById(R.id.atras);
        fab = findViewById(R.id.fab);
        id = getIntent().getStringExtra("id_edificio");
        nombre = getIntent().getStringExtra("nombre_edificio");
        medicionid = getIntent().getStringExtra("id_medicion");
        nombremedicion = getIntent().getStringExtra("nombre_medicion");
        mesnumori = getIntent().getStringExtra("mesnum");
        mesori = getIntent().getStringExtra("mes");
        añorecibir = getIntent().getStringExtra("anore");
        medid = Integer.parseInt(medicionid);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                Intent intent = new Intent(MedicionDepartamentoActivity.this, MedicionActivity.class);
                intent.putExtra("id_edificio", id);
                intent.putExtra("nombre_edificio", nombre);
                startActivity(intent);
                finish();
            }
        });
        System.out.println("mesnumori: "+mesnumori + " mesori: "+mesori);
        ArrayList<String> listaños = new ArrayList<>();
        listaños.add("2020");
        listaños.add("2021");
        listaños.add("2022");
        listaños.add("2023");
        listaños.add("2024");
        listaños.add("2025");
        listaños.add("2026");
        listaños.add("2027");
        listaños.add("2028");
        listaños.add("2029");
        listaños.add("2030");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MedicionDepartamentoActivity.this, R.layout.support_simple_spinner_dropdown_item, listaños);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selec = spin.getAdapter().getItem(position).toString();
                //spin.setVisibility(View.GONE);
                //edtaño.setVisibility(View.VISIBLE);
                añoenviar = selec;
                edtaño.setText(añoenviar);
                añoutilizar = añoenviar;
                if(!mesutilizar.isEmpty()){
                    System.out.println("Consultando: " + mesutilizar+" -/- "+añoutilizar);
                    aplicarMes(mesutilizar,añoutilizar);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtaño.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spin.setVisibility(View.VISIBLE);
                edtaño.setVisibility(View.GONE);
            }
        });

        //Obtener fehca
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        //Separar dia y año
        String[] parts = formattedDate.split("-");
        String di = parts[0];
        String me = parts[1];
        an = parts[2];

        //Obtener el mes actual
        Month mes = LocalDate.now().getMonth();
        //Convertir el numero del mes en nombre
        String nombremes = mes.getDisplayName(TextStyle.FULL, new Locale("es", "ES"));
        //Convierte a mayúscula la primera letra del mes.
        String primeraLetra = nombremes.substring(0, 1);
        String mayuscula = primeraLetra.toUpperCase();
        String demasLetras = nombremes.substring(1, nombremes.length());
        nombremes = mayuscula + demasLetras;
        mesenviar = nombremes;

        param1 = id;
        param2 = medicionid;
        param3 = me + "-" + an;

        System.out.println("mesnumori: " + mesnumori);
        if (mesnumori.equals("0")) {
            System.out.println("IF mesnumori: " + mesnumori);
            textTitulo.setText("Medicion de " + nombremedicion);
            textdireccion.setText(nombremes + "-" + an + " / " + nombre);
            añoenviar = an;
            System.out.println("an: "+an+" añoenviar: "+añoenviar);
            //edtaño.setText(añoenviar);
            if(añoenviar.equals("2020")){
                añ=0;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2021")){
                añ=1;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2022")){
                añ=2;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2023")){
                añ=3;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2024")){
                añ=4;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2025")){
                añ=5;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2026")){
                añ=6;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2027")){
                añ=7;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2028")){
                añ=8;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2029")){
                añ=9;
                spin.setSelection(añ);
            }else if(añoenviar.equals("2030")){
                añ=10;
                spin.setSelection(añ);
            }
            mesanooo = (nombremes + "-" + añoenviar);
            mesnum = me;
            mesutilizar = me;
            añoutilizar = añoenviar;
            int m = Integer.parseInt(me);
            switch (m) {
                case 1:
                    lyen.setBackgroundResource(R.drawable.shape_gradient_card);
                    tven.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 2:
                    lyfeb.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvfeb.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 3:
                    lymar.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvmar.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 4:
                    lyabr.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvabr.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 5:
                    lymay.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvmay.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 6:
                    lyjun.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvjun.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 7:
                    lyjul.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvjul.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 8:
                    lyagos.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvagos.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 9:
                    lysep.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvsep.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 10:
                    lyoct.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvoct.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 11:
                    lynov.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvnov.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 12:
                    lydic.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvdic.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
        } else {
            textTitulo.setText("Medicion de " + nombremedicion);
            textdireccion.setText(mesori + " / " + nombre);
            System.out.println("ELSE mesnumori: " + mesnumori);
            int nume = Integer.parseInt(mesnumori);
            mesanooo = (mesori);
            mesnum = mesnumori;
            System.out.println("Añoreci: "+añorecibir);
            if(añorecibir==null){
                añorecibir = an;
            }
            if(añorecibir.equals("2020")){
                añ=0;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2021")){
                añ=1;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2022")){
                añ=2;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2023")){
                añ=3;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2024")){
                añ=4;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2025")){
                añ=5;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2026")){
                añ=6;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2027")){
                añ=7;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2028")){
                añ=8;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2029")){
                añ=9;
                spin.setSelection(añ);
            }else if(añorecibir.equals("2030")){
                añ=10;
                spin.setSelection(añ);
            }
            //edtaño.setText(añorecibir);
            mesutilizar = mesnum;
            switch (nume) {
                case 1:
                    lyen.setBackgroundResource(R.drawable.shape_gradient_card);
                    tven.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 2:
                    lyfeb.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvfeb.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 3:
                    lymar.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvmar.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 4:
                    lyabr.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvabr.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 5:
                    lymay.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvmay.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 6:
                    lyjun.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvjun.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 7:
                    lyjul.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvjul.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 8:
                    lyagos.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvagos.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 9:
                    lysep.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvsep.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 10:
                    lyoct.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvoct.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 11:
                    lynov.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvnov.setTextColor(getResources().getColor(R.color.white));
                    break;
                case 12:
                    lydic.setBackgroundResource(R.drawable.shape_gradient_card);
                    tvdic.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
        }

        medi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MedicionDepartamentoActivity.this, MedicionActivity.class);
                intent.putExtra("id_edificio", id);
                intent.putExtra("nombre_edificio", nombre);
                startActivity(intent);
                finish();
            }
        });

        hom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.tieneAccesoInternet(MedicionDepartamentoActivity.this)) {
                    Intent intent = new Intent(MedicionDepartamentoActivity.this, PrincipalActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MedicionDepartamentoActivity.this, "No puede cambiar de edificio en modo Off-Line", Toast.LENGTH_SHORT).show();
                }
            }
        });

        calen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.tieneAccesoInternet(MedicionDepartamentoActivity.this)) {
                    contentcalendar.setVisibility(View.VISIBLE);
                    calendarcolor.setVisibility(View.VISIBLE);
                    calen.setVisibility(View.GONE);
                } else {
                    Toast.makeText(MedicionDepartamentoActivity.this, "Opcion no disponible en modo Off-Line", Toast.LENGTH_SHORT).show();
                }
            }
        });

        calendarcolor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentcalendar.setVisibility(View.GONE);
                calendarcolor.setVisibility(View.GONE);
                calen.setVisibility(View.VISIBLE);
            }
        });

        //logica de consulta para cada mes (actualiza la lista)
        en.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Enero";
                textdireccion.setText("Enero" + "-" + an + " / " + nombre);
                mesanooo = (mesenviar + "-" + an);
                lyen.setBackgroundResource(R.drawable.shape_gradient_card);
                tven.setTextColor(getResources().getColor(R.color.white));
                mesnum = "01";
                mesutilizar = mesnum;
                aplicarMes("01",añoutilizar);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(getResources().getColor(R.color.purpleligth));

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);


                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);


                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);


                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        feb.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Febrero";
                textdireccion.setText("Febrero" + "-" + an + " / " + nombre);
                mesanooo = (mesenviar + "-" + an);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_gradient_card);
                tvfeb.setTextColor(getResources().getColor(R.color.white));
                mesnum = "02";
                mesutilizar = mesnum;
                aplicarMes("02",añoutilizar);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);


                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);


                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);


                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        mar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Marzo";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Marzo" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_gradient_card);
                tvmar.setTextColor(getResources().getColor(R.color.white));
                mesnum = "03";
                mesutilizar = mesnum;
                aplicarMes("03",añoutilizar);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);


                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);


                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);


                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        abr.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Abril";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Abril" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_gradient_card);
                tvabr.setTextColor(getResources().getColor(R.color.white));
                mesnum = "04";
                mesutilizar = mesnum;
                aplicarMes("04",añoutilizar);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);


                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);


                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);


                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        may.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Mayo";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Mayo" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_gradient_card);
                tvmay.setTextColor(getResources().getColor(R.color.white));
                mesnum = "05";
                mesutilizar = mesnum;
                aplicarMes("05",añoutilizar);


                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);


                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);


                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        jun.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Junio";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Junio" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_gradient_card);
                tvjun.setTextColor(getResources().getColor(R.color.white));
                mesnum = "06";
                mesutilizar = mesnum;
                aplicarMes("06",añoutilizar);

                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);


                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        jul.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Julio";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Julio" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);

                lyjul.setBackgroundResource(R.drawable.shape_gradient_card);
                tvjul.setTextColor(getResources().getColor(R.color.white));
                mesnum = "07";
                mesutilizar = mesnum;
                aplicarMes("07",añoutilizar);

                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);


                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);


            }
        });

        agos.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Agosto";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Agosto" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);

                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);

                lyagos.setBackgroundResource(R.drawable.shape_gradient_card);
                tvagos.setTextColor(getResources().getColor(R.color.white));
                mesnum = "08";
                mesutilizar = mesnum;
                aplicarMes("08",añoutilizar);

                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);
            }
        });

        sep.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Septiembre";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Septiembre" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);

                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);

                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);

                lysep.setBackgroundResource(R.drawable.shape_gradient_card);
                tvsep.setTextColor(getResources().getColor(R.color.white));
                mesnum = "09";
                mesutilizar = mesnum;
                aplicarMes("09",añoutilizar);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);
            }
        });

        oct.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Octubre";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Octubre" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);

                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);

                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);

                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_gradient_card);
                tvoct.setTextColor(getResources().getColor(R.color.white));
                mesnum = "10";
                mesutilizar = mesnum;
                aplicarMes("10",añoutilizar);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);


                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);
            }
        });

        nov.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Noviembre";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Noviembre" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);

                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);

                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);

                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_gradient_card);
                tvnov.setTextColor(getResources().getColor(R.color.white));
                mesnum = "11";
                mesutilizar = mesnum;
                aplicarMes("11",añoutilizar);

                lydic.setBackgroundResource(R.drawable.shape_card_white);
                tvdic.setTextColor(R.color.purpleligth);
            }
        });

        dic.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                mesenviar = "Diciembre";
                mesanooo = (mesenviar + "-" + an);
                textdireccion.setText("Diciembre" + "-" + an + " / " + nombre);
                lyen.setBackgroundResource(R.drawable.shape_card_white);
                tven.setTextColor(R.color.purpleligth);

                lyfeb.setBackgroundResource(R.drawable.shape_card_white);
                tvfeb.setTextColor(R.color.purpleligth);

                lymar.setBackgroundResource(R.drawable.shape_card_white);
                tvmar.setTextColor(R.color.purpleligth);

                lyabr.setBackgroundResource(R.drawable.shape_card_white);
                tvabr.setTextColor(R.color.purpleligth);

                lymay.setBackgroundResource(R.drawable.shape_card_white);
                tvmay.setTextColor(R.color.purpleligth);

                lyjun.setBackgroundResource(R.drawable.shape_card_white);
                tvjun.setTextColor(R.color.purpleligth);

                lyjul.setBackgroundResource(R.drawable.shape_card_white);
                tvjul.setTextColor(R.color.purpleligth);

                lyagos.setBackgroundResource(R.drawable.shape_card_white);
                tvagos.setTextColor(R.color.purpleligth);

                lysep.setBackgroundResource(R.drawable.shape_card_white);
                tvsep.setTextColor(R.color.purpleligth);


                lyoct.setBackgroundResource(R.drawable.shape_card_white);
                tvoct.setTextColor(R.color.purpleligth);


                lynov.setBackgroundResource(R.drawable.shape_card_white);
                tvnov.setTextColor(R.color.purpleligth);

                lydic.setBackgroundResource(R.drawable.shape_gradient_card);
                tvdic.setTextColor(getResources().getColor(R.color.white));
                mesnum = "12";
                mesutilizar = mesnum;
                aplicarMes("12",añoutilizar);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final androidx.appcompat.app.AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(MedicionDepartamentoActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Tienes "+total+" registros sin guardar")
                        .setMessage("¿Seguro que desea subir los registros?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                subirRegistrosOffline();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        DisplayProgressDialog();
        iniciarServicios();
    }

    /*private void DialogCustomOnliYear() {
        DatePickerDialog monthDatePickerDialog = new DatePickerDialog(MedicionDepartamentoActivity.this, AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                System.out.println(year);
                añoenviar = String.valueOf(year);
                edtaño.setText(añoenviar);
                añoutilizar = añoenviar;
                if(!mesutilizar.isEmpty()){
                    System.out.println("Consultando: " + mesutilizar+" -/- "+añoutilizar);
                    aplicarMes(mesutilizar,añoutilizar);
                }
            }
        }, anio, m, dia) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                getDatePicker().findViewById(getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
                getDatePicker().findViewById(getResources().getIdentifier("month", "id", "android")).setVisibility(View.GONE);

            }
        };
        //monthDatePickerDialog.setTitle("Seleccione el año");
        monthDatePickerDialog.show();
    }*/

    private void obtenerActividades() {
        List<String> listactividades = new ArrayList<String>();
        for (int i = 0; i < LocalMock.getAños().size(); i++) {
            Log.d("aca sub", LocalMock.getAños().get(i).getAño());
            listaactividadd.add(LocalMock.getAños().get(i));
            listactividades.add(LocalMock.getAños().get(i).getAño());
        }
        String[] stringListCat = listactividades.toArray(new String[0]);
        ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(MedicionDepartamentoActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
        spin.setAdapter(adapterCat);
        spin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                añoenviar = listaactividadd.get(i).getAño();
                //edtaño.setText(añoenviar);
                añoutilizar = añoenviar;
                if(!mesutilizar.isEmpty()){
                    System.out.println("Consultando: " + mesutilizar+" -/- "+añoutilizar);
                    aplicarMes(mesutilizar,añoutilizar);
                }
            }
        });

        /*acactividad.setAdapter(new ArrayAdapter<String>(BitacoraActivity.this, android.R.layout.simple_spinner_dropdown_item, stringListCat));
        acactividad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String categoria = (String) acactividad.getSelectedItem();
                System.out.println(categoria);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // No seleccionaron nada
            }
        });*/
    }
    
    private void iniciarServicios() {
        if(NetworkUtils.tieneAccesoInternet(MedicionDepartamentoActivity.this)) {
            System.out.println("Con Internet");
            if(mesnumori.equals("0")){
                System.out.println("If : " + mesnumori);
                listarMediciones(param1, param2, param3);
            }else{
                System.out.println("Else : " + mesnumori);
                aplicarMes(mesnumori,añoutilizar);
            }
            consultarRegistrossinConecxion();
        } else {
            System.out.println("Sin Internet");
            int idmedicion = Integer.parseInt(medicionid);
            switch(idmedicion){
                case 1:
                    for (int i = 0; i < database.medicionDepartDao2().getAll().size(); i++) {
                        ResponseMedicionDepart entity = new ResponseMedicionDepart(
                                database.medicionDepartDao2().getAll().get(i).getMeasurement_id(),
                                database.medicionDepartDao2().getAll().get(i).getNumber_estate(),
                                database.medicionDepartDao2().getAll().get(i).getReading_month_past(),
                                database.medicionDepartDao2().getAll().get(i).getReading_month_now(),
                                database.medicionDepartDao2().getAll().get(i).getImage_attached(),
                                database.medicionDepartDao2().getAll().get(i).getIs_closed());
                        listmeiciondepaOffline.add(entity);
                    }

                    adapter = new MedicionDepaAdapter(listmeiciondepaOffline);
                    adapter.setIdMedicion(medid);
                    adapter.setEventos(eventos());
                    adapter.setEventoseditar(editar());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MedicionDepartamentoActivity.this);
                    recydepartamentos.setLayoutManager(linearLayoutManager);
                    recydepartamentos.setHasFixedSize(true);
                    recydepartamentos.setAdapter(adapter);
                    break;
                case 2:

                    for (int i = 0; i < database.medicionDepartDao().getAll().size(); i++) {
                        ResponseMedicionDepart entity = new ResponseMedicionDepart(
                                database.medicionDepartDao().getAll().get(i).getMeasurement_id(),
                                database.medicionDepartDao().getAll().get(i).getNumber_estate(),
                                database.medicionDepartDao().getAll().get(i).getReading_month_past(),
                                database.medicionDepartDao().getAll().get(i).getReading_month_now(),
                                database.medicionDepartDao().getAll().get(i).getImage_attached(),
                                database.medicionDepartDao().getAll().get(i).getIs_closed());
                        listmeiciondepaOffline.add(entity);
                    }

                    adapter = new MedicionDepaAdapter(listmeiciondepaOffline);
                    adapter.setIdMedicion(medid);
                    adapter.setEventos(eventos());
                    adapter.setEventoseditar(editar());
                    LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(MedicionDepartamentoActivity.this);
                    recydepartamentos.setLayoutManager(linearLayoutManager2);
                    recydepartamentos.setHasFixedSize(true);
                    recydepartamentos.setAdapter(adapter);

                    break;
            }
            //System.out.println(database.medicionDepartDao().getAll().get(1).getNumber_estate());
        }
    }

    private void aplicarMes(String mes, String ani) {
        pDialog.show();
        System.out.println("Mes: " + mes);
        if(ani==null){
            ani = añorecibir;
        }
        param3 = mes+"-"+ani;
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("building_id", param1)
                .addFormDataPart("measurement_type_id", param2)
                .addFormDataPart("code_period", param3)
                .build();

        System.out.println(param1+" "+param2+" "+param3+" "+token);
        Call<ArrayList<ResponseMedicionDepart>> callSelecconarCliente = service.obtenerMedicionDepa("Bearer " + token, requestBody);
        callSelecconarCliente.enqueue(new Callback<ArrayList<ResponseMedicionDepart>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseMedicionDepart>> call, Response<ArrayList<ResponseMedicionDepart>> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("responseResumen date: "+response);
                switch (response.code()){
                    case 200:
                        listmeiciondepa = response.body();
                        sinregistro.setVisibility(View.GONE);
                        recydepartamentos.setVisibility(View.VISIBLE);
                        adapter = new MedicionDepaAdapter(listmeiciondepa);
                        adapter.setIdMedicion(medid);
                        adapter.setEventos(eventos());
                        adapter.setEventoseditar(editar());
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MedicionDepartamentoActivity.this);
                        recydepartamentos.setLayoutManager(linearLayoutManager);
                        recydepartamentos.setHasFixedSize(true);
                        recydepartamentos.setAdapter(adapter);
                        break;
                    case 204:
                        recydepartamentos.setVisibility(View.GONE);
                        sinregistro.setVisibility(View.VISIBLE);
                        break;
                    case 400:
                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.apply();
                        Intent intent = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                    case 404:
                        Toast.makeText(MedicionDepartamentoActivity.this, "Error al obtener mediciones", Toast.LENGTH_SHORT).show();
                        recydepartamentos.setVisibility(View.GONE);
                        sinregistro.setVisibility(View.VISIBLE);
                        break;
                    case 429:
                        SharedPreferences prefs1 = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = prefs1.edit();
                        editor1.clear();
                        editor1.apply();
                        Intent intent1 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                        startActivity(intent1);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseMedicionDepart>> call, Throwable t) {
                Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(MedicionDepartamentoActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MedicionDepartamentoActivity.this, MedicionActivity.class);
        intent.putExtra("id_edificio", id);
        intent.putExtra("nombre_edificio", nombre);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    public void listarMediciones(String parm1, String parm2, String parm3){
        pDialog.show();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("building_id", parm1)
                .addFormDataPart("measurement_type_id", parm2)
                .addFormDataPart("code_period", parm3)
                .build();

        System.out.println(parm1+" "+parm2+" "+parm3+" "+token);
        Call<ArrayList<ResponseMedicionDepart>> callSelecconarCliente = service.obtenerMedicionDepa("Bearer " + token, requestBody);
        callSelecconarCliente.enqueue(new Callback<ArrayList<ResponseMedicionDepart>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseMedicionDepart>> call, Response<ArrayList<ResponseMedicionDepart>> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("responseResumen: "+response);

                switch (response.code()){
                    case 200:
                        listmeiciondepa = response.body();
                        adapter = new MedicionDepaAdapter(listmeiciondepa);
                        adapter.setIdMedicion(medid);
                        adapter.setEventos(eventos());
                        adapter.setEventoseditar(editar());
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MedicionDepartamentoActivity.this);
                        recydepartamentos.setLayoutManager(linearLayoutManager);
                        recydepartamentos.setHasFixedSize(true);
                        recydepartamentos.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        break;
                    case 204:
                        recydepartamentos.setVisibility(View.GONE);
                        sinregistro.setVisibility(View.VISIBLE);
                        break;
                    case 400:
                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.apply();
                        Intent intent = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                    case 404:
                        Toast.makeText(MedicionDepartamentoActivity.this, "Error al obtener mediciones", Toast.LENGTH_SHORT).show();
                        recydepartamentos.setVisibility(View.GONE);
                        sinregistro.setVisibility(View.VISIBLE);
                        break;
                    case 429:
                        SharedPreferences prefs1 = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = prefs1.edit();
                        editor1.clear();
                        editor1.apply();
                        Intent intent1 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                        startActivity(intent1);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseMedicionDepart>> call, Throwable t) {
                SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                //Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // 0=Editar, 1= No editar
    private SimpleRow eventos() {
        SimpleRow s = new SimpleRow() {
            @Override
            public void onClic(int position) {
                if(NetworkUtils.tieneAccesoInternet(MedicionDepartamentoActivity.this)) {
                    ResponseMedicionDepart listDepaOn = listmeiciondepa.get(position);

                    Intent intent = new Intent(MedicionDepartamentoActivity.this, RegistroMedicionActivity.class);
                    intent.putExtra("iddepartamento", String.valueOf(listDepaOn.getMeasurement_id()));
                    intent.putExtra("numerodepa", listDepaOn.getNumber_estate());
                    intent.putExtra("id_edificio", id);
                    intent.putExtra("nombre_edificio", nombre);
                    intent.putExtra("id_medicion", medicionid);
                    intent.putExtra("nombre_medicion", nombremedicion);
                    intent.putExtra("mesano", mesanooo);
                    intent.putExtra("mesnum", mesnum);
                    intent.putExtra("anore",añoenviar);
                    System.out.println("Año enviar 1: " +añoenviar);
                    startActivity(intent);
                    finish();
                    /*switch (listDepaOn.getIs_closed()){
                        case 0:
                            Intent intent = new Intent(MedicionDepartamentoActivity.this, EditarRegistroActivity.class);
                            intent.putExtra("iddepartamento", String.valueOf(listDepaOn.getMeasurement_id()));
                            intent.putExtra("numerodepa", listDepaOn.getNumber_estate());
                            intent.putExtra("id_edificio", id);
                            intent.putExtra("nombre_edificio", nombre);
                            intent.putExtra("id_medicion", medicionid);
                            intent.putExtra("nombre_medicion", nombremedicion);
                            intent.putExtra("image", listDepaOn.getImage_attached());
                            intent.putExtra("medi", listDepaOn.getReading_month_now());
                            startActivity(intent);
                            finish();
                        break;

                        case 1:
                            Intent intent = new Intent(MedicionDepartamentoActivity.this, RegistroMedicionActivity.class);
                            intent.putExtra("iddepartamento", String.valueOf(listDepaOn.getMeasurement_id()));
                            intent.putExtra("numerodepa", listDepaOn.getNumber_estate());
                            intent.putExtra("id_edificio", id);
                            intent.putExtra("nombre_edificio", nombre);
                            intent.putExtra("id_medicion", medicionid);
                            intent.putExtra("nombre_medicion", nombremedicion);
                            startActivity(intent2);
                            finish();
                            break;
                    }*/
                }else{
                    ResponseMedicionDepart listDepaOff = listmeiciondepaOffline.get(position);
                    Intent intent = new Intent(MedicionDepartamentoActivity.this, RegistroMedicionActivity.class);
                    intent.putExtra("iddepartamento", String.valueOf(listDepaOff.getMeasurement_id()));
                    intent.putExtra("numerodepa", listDepaOff.getNumber_estate());
                    intent.putExtra("id_edificio", id);
                    intent.putExtra("nombre_edificio", nombre);
                    intent.putExtra("id_medicion", medicionid);
                    intent.putExtra("nombre_medicion", nombremedicion);
                    intent.putExtra("mesano", mesanooo);
                    intent.putExtra("mesnum", mesnum);
                    intent.putExtra("anore",añoenviar);
                    System.out.println("Año enviar 1: " +añoenviar);
                    startActivity(intent);
                    finish();
                    /*switch (listDepaOff.getIs_closed()){
                        case 0:
                            Intent intent = new Intent(MedicionDepartamentoActivity.this, EditarRegistroActivity.class);
                            intent.putExtra("iddepartamento", String.valueOf(listDepaOff.getMeasurement_id()));
                            intent.putExtra("numerodepa", listDepaOff.getNumber_estate());
                            intent.putExtra("id_edificio", id);
                            intent.putExtra("nombre_edificio", nombre);
                            intent.putExtra("id_medicion", medicionid);
                            intent.putExtra("nombre_medicion", nombremedicion);
                            intent.putExtra("image", listDepaOff.getImage_attached());
                            intent.putExtra("medi", listDepaOff.getReading_month_now());
                            startActivity(intent);
                            finish();
                            break;

                        case 1:
                            Intent intent = new Intent(MedicionDepartamentoActivity.this, RegistroMedicionActivity.class);
                            intent.putExtra("iddepartamento", String.valueOf(listDepaOff.getMeasurement_id()));
                            intent.putExtra("numerodepa", listDepaOff.getNumber_estate());
                            intent.putExtra("id_edificio", id);
                            intent.putExtra("nombre_edificio", nombre);
                            intent.putExtra("id_medicion", medicionid);
                            intent.putExtra("nombre_medicion", nombremedicion);
                            startActivity(intent);
                            finish();
                            break;
                    }*/
                }
            }
        };
        return s;
    }

    private SimpleRowEditar editar() {
        SimpleRowEditar e = new SimpleRowEditar() {
            @Override
            public void onClic(int position) {
                if(NetworkUtils.tieneAccesoInternet(MedicionDepartamentoActivity.this)) {
                    ResponseMedicionDepart listDepaOn = listmeiciondepa.get(position);
                    Intent intent = new Intent(MedicionDepartamentoActivity.this, EditarRegistroActivity.class);
                    intent.putExtra("iddepartamento", String.valueOf(listDepaOn.getMeasurement_id()));
                    intent.putExtra("numerodepa", listDepaOn.getNumber_estate());
                    intent.putExtra("id_edificio", id);
                    intent.putExtra("nombre_edificio", nombre);
                    intent.putExtra("id_medicion", medicionid);
                    intent.putExtra("nombre_medicion", nombremedicion);
                    intent.putExtra("image", listDepaOn.getImage_attached());
                    intent.putExtra("medi", listDepaOn.getReading_month_now());
                    intent.putExtra("mesano", mesanooo);
                    intent.putExtra("mesnum", mesnum);
                    System.out.println("mesano1: "+mesanooo);
                    System.out.println("mesnum1: "+mesnum);
                    intent.putExtra("anore",añoenviar);
                    System.out.println("Año enviar 1: " +añoenviar);
                    startActivity(intent);
                    finish();
                }else{
                    ResponseMedicionDepart listDepaOff = listmeiciondepaOffline.get(position);
                    Intent intent = new Intent(MedicionDepartamentoActivity.this, EditarRegistroActivity.class);
                    intent.putExtra("iddepartamento", String.valueOf(listDepaOff.getMeasurement_id()));
                    intent.putExtra("numerodepa", listDepaOff.getNumber_estate());
                    intent.putExtra("id_edificio", id);
                    intent.putExtra("nombre_edificio", nombre);
                    intent.putExtra("id_medicion", medicionid);
                    intent.putExtra("nombre_medicion", nombremedicion);
                    intent.putExtra("image", listDepaOff.getImage_attached());
                    intent.putExtra("medi", listDepaOff.getReading_month_now());
                    intent.putExtra("mesano", mesanooo);
                    intent.putExtra("mesnum", mesnum);
                    System.out.println("mesano2: "+mesanooo);
                    intent.putExtra("anore",añoenviar);
                    System.out.println("Año enviar 2: " +añoenviar);
                    startActivity(intent);
                    finish();
                }
            }
        };
        return e;
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

    //consulta si hay registros guardados y si se encuentran en espera o subidos para actualizar
    private void consultarRegistrossinConecxion(){
        total = registroDao.getAll().size();
        if(total > 0){
            System.out.println("Tiene " + total + " registros guardados Offline");
            fab.setVisibility(View.VISIBLE);
        }else{
            fab.setVisibility(View.GONE);
            System.out.println("No hay registros sin enviar");
        }
    }

    private void subirRegistrosOffline(){
        int enviados = 0, pendientes = 0;
        /*try {
            System.out.println("Try MedicionDepartamento");
            for (i = 0; i < database.registroDao().getAll().size(); i++) {
                //Verificar registros en espera para subirlos
                if (database.registroDao().getAll().get(i).getEstado() == 1) {
                    System.out.println(" Reg id: " + database.registroDao().getAll().get(i).getUid());
                    System.out.println("Reg nonth: " + database.registroDao().getAll().get(i).getReading_month_now());
                    System.out.println("Reg mesurementid: " + database.registroDao().getAll().get(i).getMeasurementid());
                    System.out.println("Reg estado: " + database.registroDao().getAll().get(i).getEstado());
                    pDialog.show();
                    int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                    requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: " + response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                    registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

        }catch (Exception e){
            System.out.println("Catch MedicionDepartamento: " + e);
            for (i = 0; i < database.registroDao().getAll().size() - 1; i++) {
                *//*if (database.registroDao().getAll().get(i).getEstado() == 1) {
                    pendientes++;
                    System.out.print("Registros pendientes: " + pendientes);
                } else {
                    enviados++;
                    System.out.print("Registros enviados: " + enviados);
                }*//*
                if (database.registroDao().getAll().get(i).getEstado() == 1) {
                    System.out.println(" Reg id: " + database.registroDao().getAll().get(i).getUid());
                    System.out.println("Reg nonth: " + database.registroDao().getAll().get(i).getReading_month_now());
                    System.out.println("Reg mesurementid: " + database.registroDao().getAll().get(i).getMeasurementid());
                    System.out.println("Reg estado: " + database.registroDao().getAll().get(i).getEstado());
                    pDialog.show();
                    int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                    requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: " + response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                    registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }*/

        pDialogg = new ProgressDialog(MedicionDepartamentoActivity.this);
        pDialogg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialogg.setMessage("Procesando...");
        pDialogg.setCancelable(false);
        //pDialogg.setMax(100);
        uploadOfflineAsincrona = new UploadOfflineAsincrona();
        uploadOfflineAsincrona.execute();
    }

    private void tareaLarga(){
        try {
            Thread.sleep(5000);
            iniciarServicios();
        } catch(InterruptedException e) {}
    }

    private class UploadOfflineAsincrona extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
            System.out.println("Try MedicionDepartamento");
            List<RegistroEntity> reg = database.registroDao().getAll();
            for(RegistroEntity registro: reg){
                if (registro.getEstado() == 1) {
                    //tareaLarga();
                    //publishProgress(i*10);
                    System.out.println("Reg id fornew: " + registro.getUid());
                    System.out.println("Reg nonth fornew: " + registro.getReading_month_now());
                    System.out.println("Reg mesurementid fornew: " + registro.getMeasurementid());
                    //System.out.println("Photo: " + database.registroDao().getAll().get(i).getImage_attached());
                    System.out.println("Reg estado fornew: " + registro.getEstado());
                    //pDialog.show();
                    int measurementid = registro.getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(registro.getReading_month_now());
                    requestUpdateRegistro.setImage_attached(registro.getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: " + response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,registro.getUid());
                                    registroDao.queryDelete(registro.getUid());
                                    System.out.println("queryDelete: "+registro.getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+registro.getUid()+" no se guardo.");
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            /*for (i = 0; i < database.registroDao().getAll().size(); i++) {
                if (database.registroDao().getAll().get(i).getEstado() == 1) {
                    //tareaLarga();
                    publishProgress(i*10);
                    System.out.println("Reg id: " + database.registroDao().getAll().get(i).getUid());
                    System.out.println("Reg nonth: " + database.registroDao().getAll().get(i).getReading_month_now());
                    System.out.println("Reg mesurementid: " + database.registroDao().getAll().get(i).getMeasurementid());
                    //System.out.println("Photo: " + database.registroDao().getAll().get(i).getImage_attached());
                    System.out.println("Reg estado: " + database.registroDao().getAll().get(i).getEstado());
                    //pDialog.show();
                    int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                    requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: " + response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                    registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }*/

        }catch (Exception e){
            System.out.println("Catch MedicionDepartamento: " + e);
            for (i = 0; i < database.registroDao().getAll().size() - 1; i++) {
               /* if (database.registroDao().getAll().get(i).getEstado() == 1) {
                    pendientes++;
                    System.out.print("Registros pendientes: " + pendientes);
                } else {
                    enviados++;
                    System.out.print("Registros enviados: " + enviados);
                }*/
                if (database.registroDao().getAll().get(i).getEstado() == 1) {
                    tareaLarga();
                    publishProgress(i*10);
                    System.out.println(" Reg id: " + database.registroDao().getAll().get(i).getUid());
                    System.out.println("Reg nonth: " + database.registroDao().getAll().get(i).getReading_month_now());
                    System.out.println("Reg mesurementid: " + database.registroDao().getAll().get(i).getMeasurementid());
                    System.out.println("Reg estado: " + database.registroDao().getAll().get(i).getEstado());
                    //pDialog.show();
                    int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                    requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: " + response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                    registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(MedicionDepartamentoActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(MedicionDepartamentoActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(MedicionDepartamentoActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }

            return true;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialogg.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pDialogg.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    UploadOfflineAsincrona.this.cancel(true);
                }
            });
            pDialogg.setProgress(0);
            pDialogg.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                pDialogg.dismiss();
                Toast.makeText(MedicionDepartamentoActivity.this, "¡Actualización de registros Off-Line finalizada!", Toast.LENGTH_SHORT).show();
                registroDao.deleteAll();
                consultarRegistrossinConecxion();
                tareaLarga();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MedicionDepartamentoActivity.this, "¡Actualización de registros Off-Line cancelada!", Toast.LENGTH_SHORT).show();
        }
    }
}
