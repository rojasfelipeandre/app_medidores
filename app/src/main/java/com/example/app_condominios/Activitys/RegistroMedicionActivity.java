package com.example.app_condominios.Activitys;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Models.Request.RequestUpdateRegistro;
import com.example.app_condominios.Models.Response.ResponseRegistroMedicion;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.MostrarFotoTask;
import com.example.app_condominios.Util.RedInternet.ConexionServicio;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;
import com.example.app_condominios.Util.Util;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.app_condominios.Util.Constants.compressImage;


public class RegistroMedicionActivity extends AppCompatActivity {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private SharedPreferences prefs;
    private ImageView back,view;
    private LinearLayout tomarfoto, editarfoto, guardar1, guardar2;
    private EditText registro;
    private TextView numerdepa,textmesano;
    private TextInputLayout tireg;
    private String id, nombre, mesuranmentid;
    private ProgressDialog pDialog;
    private String tmpRuta, token,iddepa,medicionid,nombremedicion,phatImage,numdepa,mesnum;
    AppDatabase database;
    RegistroDao registroDao;
    private ConexionServicio conexionServicio;
    String currentPhotoPath,mesano;
    static final int REQUEST_TAKE_PHOTO = 1;
    //private String tmpRuta = "";
    Uri photoURI;
    private String añoenviar;
    Bitmap imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_medicion);
        changeStatusBarColor();
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        token = prefs.getString(Constants.token,"");
        database = AppDatabase.getDatabaseInstance(RegistroMedicionActivity.this);
        id = getIntent().getStringExtra("id_edificio");
        numdepa = getIntent().getStringExtra("numerodepa");
        nombre = getIntent().getStringExtra("nombre_edificio");
        iddepa = getIntent().getStringExtra("iddepartamento");
        medicionid = getIntent().getStringExtra("id_medicion");
        nombremedicion = getIntent().getStringExtra("nombre_medicion");
        mesano = getIntent().getStringExtra("mesano");
        mesnum = getIntent().getStringExtra("mesnum");
        añoenviar = getIntent().getStringExtra("anore");
        System.out.println("idedificio: " + id + " iddepa: " + iddepa);
        back = findViewById(R.id.atras);
        view = findViewById(R.id.imvfoto);
        numerdepa = findViewById(R.id.textnumerodepa);
        textmesano = findViewById(R.id.textmesano);
        tomarfoto = findViewById(R.id.idtomarfoto);
        editarfoto = findViewById(R.id.ideditarfoto);
        guardar1 = findViewById(R.id.btnguardar1);
        guardar2 = findViewById(R.id.btnguardar2);
        registro = findViewById(R.id.edtregistro);
        //nomimg = findViewById(R.id.txtidimagen);
        tireg = findViewById(R.id.tireg);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistroMedicionActivity.this, MedicionDepartamentoActivity.class);
                intent.putExtra("id_edificio", id);
                intent.putExtra("nombre_edificio", nombre);
                intent.putExtra("id_medicion", medicionid);
                intent.putExtra("nombre_medicion", nombremedicion);
                intent.putExtra("mesnum", mesnum);
                intent.putExtra("mes", mesano);
                intent.putExtra("anore",añoenviar);
                startActivity(intent);
                finish();
            }
        });

        numerdepa.setText("Dpto "+numdepa +" - "+ nombre);
        textmesano.setText(mesano);

        registro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(registro.getText().toString().isEmpty()){
                    guardar2.setVisibility(View.GONE);
                }else{
                    if(tmpRuta == null){
                        guardar2.setVisibility(View.GONE);
                    }else{
                        tireg.setError(null);
                        guardar2.setVisibility(View.VISIBLE);
                        guardar1.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(registro.getText().toString().isEmpty()){
                    guardar2.setVisibility(View.GONE);
                }else{
                    if(tmpRuta == null){
                        guardar2.setVisibility(View.GONE);
                    }else{
                        guardar2.setVisibility(View.VISIBLE);
                        guardar1.setVisibility(View.GONE);
                    }

                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(registro.getText().toString().isEmpty()){
                    guardar2.setVisibility(View.GONE);
                }else{
                    if(tmpRuta == null){
                        guardar2.setVisibility(View.GONE);
                    }else{
                        guardar2.setVisibility(View.VISIBLE);
                        guardar1.setVisibility(View.GONE);
                    }

                }
            }
        });

        tomarfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    tomarfoto();
            }
        });

        editarfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarfoto();
            }
        });

        guardar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        guardar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             validar();
            }
        });

        DisplayProgressDialog();
    }

    private void validar() {
        final String compruebadecimal = registro.getEditableText().toString().trim();
        final String regex = "/(?:^|\\s)([1-9](?:\\d*|(?:\\d{0,2})(?:,\\d{3})*)(?:\\.\\d*[1-9])?|0?\\.\\d*[1-9]|0)(?:\\s|$)/";
        final String myregexp="^\\d*\\.\\d+|\\d+\\.\\d*$";
        if(registro.getText().toString().isEmpty()){
            tireg.setError("Ingresar valor del contómetro");
        }else if(!compruebadecimal.matches(myregexp)) {
            tireg.setError("Ingrese un valor decimal valido");
        }else if(phatImage == ""){
            Toast.makeText(RegistroMedicionActivity.this, "Tome una foto valida", Toast.LENGTH_SHORT).show();
        }else{
            tireg.setError(null);
            guardarRegistro();
        }
    }

    private void guardarRegistro() {
        int measurementid = Integer.parseInt(iddepa);
        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
        requestUpdateRegistro.setReading_month_now(Double.parseDouble(registro.getText().toString()));
        requestUpdateRegistro.setImage_attached("data:image/jpeg;base64,"+phatImage);
        requestUpdateRegistro.setToken(token);
        System.out.println("Imagen:  "+"data:image/jpeg;base64,"+phatImage);

        if(NetworkUtils.tieneAccesoInternet(RegistroMedicionActivity.this)){
            System.out.println("Registro con internet");
            Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid,requestUpdateRegistro);
            callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                @Override
                public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    System.out.println("responseResumen: "+response);
                    switch (response.code()){
                        case 200:
                            System.out.println(response.body().getMessage());
                            tmpRuta = "";
                            Intent intent = new Intent(RegistroMedicionActivity.this, ConfirmacionRegistroActivity.class);
                            intent.putExtra("id_medicion", medicionid);
                            intent.putExtra("nombre_medicion", nombremedicion);
                            intent.putExtra("id_edificio", id);
                            intent.putExtra("nombre_edificio", nombre);
                            intent.putExtra("measurementid", String.valueOf(measurementid));
                            intent.putExtra("mesnum", mesnum);
                            intent.putExtra("mes", mesano);
                            intent.putExtra("anore",añoenviar);
                            startActivity(intent);
                            finish();
                            break;
                        case 422:
                            Toast.makeText(RegistroMedicionActivity.this,getString(R.string.error_inesperado) , Toast.LENGTH_SHORT).show();
                            break;
                        case 400:
                            SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent2 = new Intent(RegistroMedicionActivity.this, LoginActivity.class);
                            startActivity(intent2);
                            finish();
                            break;
                        default:
                            Toast.makeText(RegistroMedicionActivity.this,getString(R.string.error_inesperado) , Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                    Toast.makeText(RegistroMedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            System.out.println("Registro sin internet");
            registroDao = database.registroDao();

            RegistroEntity entity = new RegistroEntity();
            entity.setReading_month_now(Double.parseDouble(registro.getText().toString()));
            entity.setImage_attached("data:image/jpeg;base64,"+phatImage);
            entity.setMeasurementid(measurementid);
            entity.setEstado(1); //en espera de registro a servicio
            registroDao.insert(entity);

            tmpRuta = "";
            Intent intent = new Intent(RegistroMedicionActivity.this, ConfirmacionRegistroActivity.class);
            intent.putExtra("id_medicion", medicionid);
            intent.putExtra("nombre_medicion", nombremedicion);
            intent.putExtra("id_edificio", id);
            intent.putExtra("nombre_edificio", nombre);
            intent.putExtra("measurementid", String.valueOf(measurementid));
            intent.putExtra("mesnum", mesnum);
            intent.putExtra("mes", mesano);
            intent.putExtra("anore",añoenviar);
            startActivity(intent);
            finish();
        }

    }

    private void tomarfoto() {
        if (ActivityCompat.checkSelfPermission(RegistroMedicionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(RegistroMedicionActivity.this, "No puede realizar la acción porque no se ha dado el permiso de Almacenamiento.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (Util.isExternalStorageWritable()) {
            if (Util.getAlbumStorageDir(RegistroMedicionActivity.this)) {
                File file = Util.getNuevaRutaFoto("Con", RegistroMedicionActivity.this);
                tmpRuta = file.getPath();
                System.out.println("file: " + file);
                System.out.println("tmpRuta: " + tmpRuta);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outputUri = FileProvider.getUriForFile(RegistroMedicionActivity.this, getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
                System.out.println("outputUri: " + outputUri);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = ClipData.newUri(getContentResolver(), "RegistroTask", outputUri);
                    intent.setClipData(clip);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                startActivityForResult(intent, 1);
            } else {
                Toast.makeText(RegistroMedicionActivity.this, getResources().getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(RegistroMedicionActivity.this, getResources().getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
        }
    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(RegistroMedicionActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegistroMedicionActivity.this, MedicionDepartamentoActivity.class);
        intent.putExtra("id_edificio", id);
        intent.putExtra("nombre_edificio", nombre);
        intent.putExtra("id_medicion", medicionid);
        intent.putExtra("nombre_medicion", nombremedicion);
        intent.putExtra("mesnum", mesnum);
        intent.putExtra("mes", mesano);
        intent.putExtra("anore",añoenviar);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if (requestCode == 1) {

                String Pa1 = (compressImage(RegistroMedicionActivity.this,tmpRuta));
                phatImage = (CargarFoto(Pa1));
                if (phatImage == null || phatImage == "") {
                    System.out.println("phatImagenNul: "+phatImage);
                } else {
                    System.out.println("phatImageNoNul: " + phatImage);
                    view.setVisibility(View.VISIBLE);
                    tomarfoto.setVisibility(View.GONE);
                    editarfoto.setVisibility(View.VISIBLE);

                    loadBitmap(tmpRuta, view);
                    String Pa2 = (compressImage(RegistroMedicionActivity.this,tmpRuta));
                    phatImage = (CargarFoto(Pa1));

                    System.out.println("phatImage: " + phatImage);
                    if (tmpRuta == null) {
                        System.out.println("Vacio");
                    } else {
                        if (registro.getText().toString().isEmpty()) {
                            tireg.setError("Necesitas ingresar el valor del contómetro");
                        } else {
                            guardar1.setVisibility(View.GONE);
                            guardar2.setVisibility(View.VISIBLE);
                        }

                        /*String separador = Pattern.quote("/");
                        String[] parts = tmpRuta.split(separador);
                        String part10 = parts[9];
                        System.out.println("Nme: " + part10);

                        String separador2 = Pattern.quote("_");
                        String[] parts2 = part10.split(separador2);
                        String uno = parts2[0];
                        String dos = parts2[1];
                        System.out.println("Nme2: " + dos);
                        nomimg.setText("Foto adjunta: " + dos);*/
                    }

               }
           }
        }

    }

    public String CargarFoto (String path_foto){
        String foto_str="";
        String encodedString="";
        if(path_foto!=null ){
            try {
                File file = new File(path_foto);
                try (FileInputStream fis = new FileInputStream(file)) {
                    byte[] bytes = new byte[(int) file.length()];
                    fis.read(bytes);
                    encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                    //encodedString = Base64.encodeToString(Constants.resizeImage(RegistroMedicionActivity.this,bytes,150,150), Base64.DEFAULT);
                } catch (IOException ex) {
                    encodedString = null;
                    System.out.println("Error al convertir la imagen. " + ex.getMessage() +  ex);
                }

                //foto_str = Base64.encodeToString(String.valueOf(bitmapFile),Base64.DEFAULT);
            }catch (Exception e){
                e.printStackTrace();
                encodedString = null;
                return encodedString;
            }
            Log.e("foto",foto_str);
        }
        System.out.println("encodedString: " + encodedString);
        return encodedString;
    }

    public void loadBitmap(String ruta, final ImageView imageView) {
        MostrarFotoTask mostrarFotoTask = new MostrarFotoTask(new MostrarFotoTask.OnMostrarFotoTaskCompleted() {
            @Override
            public void onMostrarFotoTaskCompleted(Bitmap bitmap) {
                if (bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                } else {
                    Toast.makeText(RegistroMedicionActivity.this, "Error al cargar la foto", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mostrarFotoTask.execute(ruta);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

}