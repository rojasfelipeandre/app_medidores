package com.example.app_condominios.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_condominios.Models.Response.ResponseLogin;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Constants;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private EditText usuario, contrasena;
    private TextInputLayout user, pass;
    private LinearLayout iniciarses1,iniciarses2;
    private TextView olvidecontra;
    private ProgressDialog pDialog;
    private String token, username, email;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        changeStatusBarColor();
        prefs = getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        usuario = findViewById(R.id.edtUser);
        contrasena = findViewById(R.id.edtPass);
        iniciarses1 = findViewById(R.id.btnIniciarsesion1);
        iniciarses2 = findViewById(R.id.btnIniciarsesion2);
        olvidecontra = findViewById(R.id.tvolvidecontraseña);
        user = findViewById(R.id.tiluser);
        pass = findViewById(R.id.tilpass);

        contrasena.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(contrasena.getText().toString().isEmpty()){
                    iniciarses1.setVisibility(View.GONE);
                }else{
                    iniciarses1.setVisibility(View.GONE);
                    iniciarses2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(contrasena.getText().toString().isEmpty()){
                    iniciarses1.setVisibility(View.GONE);
                }else{
                    iniciarses1.setVisibility(View.GONE);
                    iniciarses2.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(contrasena.getText().toString().isEmpty()){
                    iniciarses1.setVisibility(View.GONE);
                }else{
                    iniciarses1.setVisibility(View.GONE);
                    iniciarses2.setVisibility(View.VISIBLE);
                }
            }
        });

        iniciarses1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "Llene los campos requeridos", Toast.LENGTH_SHORT).show();

            }
        });

        iniciarses2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               validarCampos();
            }
        });

        olvidecontra.setPaintFlags(olvidecontra.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        olvidecontra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RecuperarContraActivity.class);
                startActivity(intent);
                finish();
            }
        });

        DisplayProgressDialog();

    }

    private void validarCampos() {
        final String compruebaemail = usuario.getEditableText().toString().trim();
        final String regex = "(?:[^<>()\\[\\].,;:\\s@\"]+(?:\\.[^<>()\\[\\].,;:\\s@\"]+)*|\"[^\\n\"]+\")@(?:[^<>()\\[\\].,;:\\s@\"]+\\.)+[^<>()\\[\\]\\.,;:\\s@\"]{2,63}";
        if(usuario.getText().toString().isEmpty()){
            user.setError("Ingrese su correo");
            pass.setError(null);
        }else if(!compruebaemail.matches(regex)) {
            user.setError("Ingrese un correo válido");
            pass.setError(null);
        }else if(contrasena.getText().toString().isEmpty()){
            user.setError(null);
            pass.setError("Necesitas ingresar tu contraseña");
        }else{
            user.setError(null);
            pass.setError(null);
            validarLogin();
        }

    }

    private void validarLogin() {
        pDialog.show();
        String user = usuario.getText().toString();
        String pas = contrasena.getText().toString();
        Call<ResponseLogin> callObtenerLogin = service.obtenerLogin(user, pas);
        callObtenerLogin.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("responseResumen: " + response);

                switch (response.code()){
                    case 200:
                    irActivity();
                    token = response.body().getAccess_token();
                    username = response.body().getUser().getName();
                    email = response.body().getUser().getEmail();
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Constants.token, response.body().getAccess_token());
                    editor.putString(Constants.name, response.body().getUser().getName());
                    editor.putString(Constants.email, response.body().getUser().getEmail());
                    editor.apply();
                    System.out.println(response.body().getAccess_token());
                    System.out.println(prefs.getString("Pref" + Constants.token,"") + " --- " + prefs.getString(Constants.name,"") + " --- " + prefs.getString(Constants.email,""));
                        break;
                    case 401:
                        Toast.makeText(LoginActivity.this, "Email o contraseña no válidos.", Toast.LENGTH_SHORT).show();
                        break;

                    case 403:
                        Toast.makeText(LoginActivity.this, "El usuario no es tipo administrador.", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Toast.makeText(LoginActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void irActivity() {
        Intent intent = new Intent(LoginActivity.this, PrincipalActivity.class);
        startActivity(intent);
        finish();
    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

}