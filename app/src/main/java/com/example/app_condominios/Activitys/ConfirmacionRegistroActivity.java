package com.example.app_condominios.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.MedicionDepartDao;
import com.example.app_condominios.Database.dao.MedicionDepartDao2;
import com.example.app_condominios.Database.entity.MedicionDepartEntity;
import com.example.app_condominios.Database.entity.MedicionDepartEntity2;
import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Commons;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmacionRegistroActivity extends AppCompatActivity {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private LinearLayout siguiente, lista;
    private SharedPreferences prefs;
    private ImageView back;
    private String id, nombre, medicionid, nombremedicion, measurementid,mesnum,mes;
    AppDatabase database;
    MedicionDepartDao medicionDepartDao;
    MedicionDepartDao2 medicionDepartDao2;
    private String token, mess, ano,añoenviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion_registro);
        database = AppDatabase.getDatabaseInstance(ConfirmacionRegistroActivity.this);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        token = prefs.getString(Constants.token,"");
        changeStatusBarColor();
        siguiente = findViewById(R.id.siguiente);
        lista = findViewById(R.id.lista);
        back = findViewById(R.id.atras);
        id = getIntent().getStringExtra("id_edificio");
        nombre = getIntent().getStringExtra("nombre_edificio");
        medicionid = getIntent().getStringExtra("id_medicion");
        nombremedicion = getIntent().getStringExtra("nombre_medicion");
        measurementid = getIntent().getStringExtra("measurementid");
        mesnum = getIntent().getStringExtra("mesnum");
        mess = getIntent().getStringExtra("mes");
        añoenviar = getIntent().getStringExtra("anore");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmacionRegistroActivity.this, MedicionDepartamentoActivity.class);
                intent.putExtra("id_medicion", medicionid);
                intent.putExtra("nombre_medicion", nombremedicion);
                intent.putExtra("id_edificio", id);
                intent.putExtra("nombre_edificio", nombre);
                intent.putExtra("mesnum", mesnum);
                intent.putExtra("mes", mess);
                intent.putExtra("anore",añoenviar);
                System.out.println("mesnum1: "+mesnum);
                System.out.println("mes1: "+mess);
                startActivity(intent);
                finish();
            }
        });
        lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmacionRegistroActivity.this, MedicionDepartamentoActivity.class);
                intent.putExtra("id_medicion", medicionid);
                intent.putExtra("nombre_medicion", nombremedicion);
                intent.putExtra("id_edificio", id);
                intent.putExtra("nombre_edificio", nombre);
                intent.putExtra("mesnum", mesnum);
                intent.putExtra("mes", mess);
                intent.putExtra("anore",añoenviar);
                System.out.println("mesnum2: "+mesnum);
                System.out.println("mes2: "+mess);
                startActivity(intent);
                finish();
            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        //Separar dia y año
        String[] parts = formattedDate.split("-");
        String di = parts[0];
        mes = parts[1];
        ano = parts[2];
        iniciarServicios();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ConfirmacionRegistroActivity.this, MedicionDepartamentoActivity.class);
        intent.putExtra("id_medicion", medicionid);
        intent.putExtra("nombre_medicion", nombremedicion);
        intent.putExtra("id_edificio", id);
        intent.putExtra("nombre_edificio", nombre);
        intent.putExtra("mesnum", mesnum);
        intent.putExtra("mes", mess);
        intent.putExtra("anore",añoenviar);
        System.out.println("mesnum3: "+mesnum);
        System.out.println("mes3: "+mess);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    private void iniciarServicios() {
        if(NetworkUtils.tieneAccesoInternet(ConfirmacionRegistroActivity.this)) {
            System.out.println("Con Internet");
            inicializarListadosDepaSinConexion();
        } else {
            actualizarrlistaregistroOffline();
        }
    }

    private void actualizarrlistaregistroOffline() {
        int mesurement = Integer.parseInt(measurementid);
        int idmed = Integer.parseInt(medicionid);
        // 2 luz = medicionDepartDao
        // 1 agua = medicionDepartDao2

        if(idmed == 2){
            database.medicionDepartDao().queryUpdate(2,mesurement);
        }else {
            database.medicionDepartDao2().queryUpdate2(2,mesurement);
        }


    }

    private void inicializarListadosDepaSinConexion() {
        String param1 = id;
        String param2 = "2";
        String param4 = "1";
        String param3 = mes+"-"+ano;

        System.out.println("consultando para llenar lista");
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("building_id", param1)
                    .addFormDataPart("measurement_type_id", param2)
                    .addFormDataPart("code_period", param3)
                    .build();

            System.out.println(param1+" "+param2+" "+param3+" "+token);
            Call<ArrayList<ResponseMedicionDepart>> callSelecconarCliente = service.obtenerMedicionDepa("Bearer " + token, requestBody);
            callSelecconarCliente.enqueue(new Callback<ArrayList<ResponseMedicionDepart>>() {
                @Override
                public void onResponse(Call<ArrayList<ResponseMedicionDepart>> call, Response<ArrayList<ResponseMedicionDepart>> response) {
                    System.out.println("responseResumen: "+response);
                    if (response.code()== 200) {
                        guardarListaLuzSinConexionSQL(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ResponseMedicionDepart>> call, Throwable t) {
                    Toast.makeText(ConfirmacionRegistroActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                }
            });


            RequestBody requestBody2 = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("building_id", param1)
                    .addFormDataPart("measurement_type_id", param4)
                    .addFormDataPart("code_period", param3)
                    .build();

            System.out.println(param1+" "+param2+" "+param3+" "+token);
            Call<ArrayList<ResponseMedicionDepart>> callSelecconarCliente2 = service.obtenerMedicionDepa("Bearer " + token, requestBody2);
            callSelecconarCliente2.enqueue(new Callback<ArrayList<ResponseMedicionDepart>>() {
                @Override
                public void onResponse(Call<ArrayList<ResponseMedicionDepart>> call, Response<ArrayList<ResponseMedicionDepart>> response) {
                    System.out.println("responseResumen: "+response);
                    if (response.code()== 200) {
                        guardarListaAguaSinConexionSQL(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ResponseMedicionDepart>> call, Throwable t) {
                    Toast.makeText(ConfirmacionRegistroActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                }
            });



        } catch (Exception e) {
            Commons.mostrarDialogo(ConfirmacionRegistroActivity.this, "AVISO", "Ocurrio un error al iniciar la aplicación, por favor intentelo nuevamente.", true, "OK", false, "", null, 0);
        }
    }

    private void guardarListaLuzSinConexionSQL(List<ResponseMedicionDepart> ListaLuz) {
        medicionDepartDao = database.medicionDepartDao();
        if (existeListaLuzSinConexion()) {
            medicionDepartDao.deleteAll();
        }
        for (int i = 0; i < ListaLuz.size(); i++) {
            MedicionDepartEntity entity = new MedicionDepartEntity(ListaLuz.get(i), i);
            medicionDepartDao.insert(entity);
        }

        System.out.println("Guardando lista de Luz en bd interna");
    }

    private void guardarListaAguaSinConexionSQL(List<ResponseMedicionDepart> ListaAgua) {
        medicionDepartDao2 = database.medicionDepartDao2();
        if (existeListaAguaSinConexion()) {
            medicionDepartDao2.deleteAll();
        }
        for (int i = 0; i < ListaAgua.size(); i++) {
            MedicionDepartEntity2 entity = new MedicionDepartEntity2(ListaAgua.get(i), i);
            medicionDepartDao2.insert(entity);
        }

        System.out.println("Guardando lista de Agua en bd interna");
    }

    private boolean existeListaLuzSinConexion() {
        System.out.println("Existe lista de Luz sin conec");
        return medicionDepartDao.getAll().size() > 0;
    }

    private boolean existeListaAguaSinConexion() {
        System.out.println("Existe lista de Agua sin conec");
        return medicionDepartDao2.getAll().size() > 0;
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

}