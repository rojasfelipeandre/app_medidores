package com.example.app_condominios.Activitys;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_condominios.R;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;

import org.w3c.dom.Text;

public class PerfilActivity extends AppCompatActivity {
    private SharedPreferences prefs;
    TextView nombre, correo;
    LinearLayout cerrarsesion;
    ImageView back, home, perfil;
    private String name, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        changeStatusBarColor();
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        name = prefs.getString(Constants.name,"");
        email = prefs.getString(Constants.email,"");
        nombre = findViewById(R.id.tvnombre);
        //dni = findViewById(R.id.tvdni);
        //cargo = findViewById(R.id.tvcargo);
        correo = findViewById(R.id.tvcorreo);
        cerrarsesion = findViewById(R.id.btncerrarses);
        back = findViewById(R.id.atras);
        home = findViewById(R.id.home);
        perfil = findViewById(R.id.perfil);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PerfilActivity.this, PrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PerfilActivity.this, PrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });
        nombre.setText(name);
        correo.setText(email);

        cerrarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(PerfilActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Cerrar sesión")
                        .setMessage("¿Seguro que desea cerrar sesión?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                cleanUp();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.create();
                builder.show();
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PerfilActivity.this, PrincipalActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @SuppressLint("NewApi")
    private void cleanUp() {
        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(PerfilActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}