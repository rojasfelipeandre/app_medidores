package com.example.app_condominios.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.Models.Response.ResponseRegistroRecuperar;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecuperarContraActivity extends AppCompatActivity {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private LinearLayout btnenviar,btnenviar2;
    private EditText edtcorreo;
    private String email;
    private ProgressDialog pDialog;
    private TextInputLayout imlcorreorec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contra);
        changeStatusBarColor();
        edtcorreo = findViewById(R.id.edtCorreo);
        btnenviar = findViewById(R.id.btnenviarcorreo);
        btnenviar2 = findViewById(R.id.btnenviarcorreo2);
        imlcorreorec =  findViewById(R.id.imlcorreorec);

        edtcorreo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(edtcorreo.getText().toString().isEmpty()){
                    btnenviar.setVisibility(View.GONE);
                }else{
                    btnenviar.setVisibility(View.GONE);
                    btnenviar2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(edtcorreo.getText().toString().isEmpty()){
                    btnenviar.setVisibility(View.GONE);
                }else{
                    btnenviar.setVisibility(View.GONE);
                    btnenviar2.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(edtcorreo.getText().toString().isEmpty()){
                    btnenviar.setVisibility(View.GONE);
                }else{
                    btnenviar.setVisibility(View.GONE);
                    btnenviar2.setVisibility(View.VISIBLE);
                }
            }
        });

        btnenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RecuperarContraActivity.this, "LLene los campos requeridos", Toast.LENGTH_SHORT).show();

            }
        });

        btnenviar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarCampos();
            }
        });
        DisplayProgressDialog();
    }

    private void validarCampos() {
        final String compruebaemail = edtcorreo.getEditableText().toString().trim();
        final String regex = "(?:[^<>()\\[\\].,;:\\s@\"]+(?:\\.[^<>()\\[\\].,;:\\s@\"]+)*|\"[^\\n\"]+\")@(?:[^<>()\\[\\].,;:\\s@\"]+\\.)+[^<>()\\[\\]\\.,;:\\s@\"]{2,63}";
        if(edtcorreo.getText().toString().isEmpty()){
            imlcorreorec.setError("Ingrese su correo");
        }else if(!compruebaemail.matches(regex)) {
            imlcorreorec.setError("Ingrese un correo válido");
        }else{
            imlcorreorec.setError(null);
            imlcorreorec.setError(null);
            validarRecuperar();
        }

    }

    private void validarRecuperar() {
        pDialog.show();
        email = edtcorreo.getText().toString();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("email", email)
                .build();

        Call<ResponseRegistroRecuperar> callSelecconarCliente = service.obtenerRecuperarContra(requestBody);
        callSelecconarCliente.enqueue(new Callback<ResponseRegistroRecuperar>() {
            @Override
            public void onResponse(Call<ResponseRegistroRecuperar> call, Response<ResponseRegistroRecuperar> response) {
                System.out.println("responseResumen: "+response);
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }

                   if(response.code() == 200) {
                       Intent intent = new Intent(RecuperarContraActivity.this, LoginActivity.class);
                       startActivity(intent);
                       finish();
                   }else{
                       Toast.makeText(RecuperarContraActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                   }

            }

            @Override
            public void onFailure(Call<ResponseRegistroRecuperar> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Toast.makeText(RecuperarContraActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(RecuperarContraActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RecuperarContraActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }
}