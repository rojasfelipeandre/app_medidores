package com.example.app_condominios.Activitys;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_condominios.Adapters.EdificiosAdapter;
import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.EdificiosDao;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.dao.TipoMedicionDao;
import com.example.app_condominios.Database.entity.EdificioEntity;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Database.entity.TipoMedicionEntity;
import com.example.app_condominios.Models.Request.RequestPadreResponseMedicionTipo;
import com.example.app_condominios.Models.Request.RequestUpdateRegistro;
import com.example.app_condominios.Models.Response.ResponseEdificios;
import com.example.app_condominios.Models.Response.ResponseMedicionTipo;
import com.example.app_condominios.Models.Response.ResponseRegistroMedicion;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Commons;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.ExpandableHeightGridView;
import com.example.app_condominios.Util.RedInternet.ConexionServicio;
import com.example.app_condominios.Util.RedInternet.ConexionViewModel;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;
import com.example.app_condominios.Util.RedInternet.ServicioConectado;
import com.example.app_condominios.Util.RedInternet.ServicioDesconectado;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrincipalActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private SharedPreferences prefs;
    private TextView nombre, fecha;
    private ImageView home, perfil;
    private ExpandableHeightGridView gridView;
    private String token, name;
    private ProgressDialog pDialog,pDialogg;
    AppDatabase database;
    EdificiosDao edificiosDao;
    TipoMedicionDao tipoMedicionDao;
    RegistroDao registroDao;
    String direccion;
    private int i, total,pendientes1,enviados1;
    private FloatingActionButton fab;
    private ArrayList<ResponseEdificios> arrayedificiosoffline = new ArrayList<>();
    private UploadOfflineAsincrona uploadOfflineAsincrona;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        changeStatusBarColor();
        database = AppDatabase.getDatabaseInstance(PrincipalActivity.this);
        registroDao = database.registroDao();
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        nombre = findViewById(R.id.nombre);
        fecha = findViewById(R.id.fecha);
        home = findViewById(R.id.home);
        perfil = findViewById(R.id.perfil);
        gridView = findViewById(R.id.gridview);
        gridView.setExpanded(true);
        token = prefs.getString(Constants.token,"");
        name = prefs.getString(Constants.name,"");
        nombre.setText("¡Bienvenido " +name+"!");
        fab = findViewById(R.id.fab);
        //Obtener fehca
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());

        //Separar dia y año
        String[] parts = formattedDate.split("-");
        String di = parts[0];
        String me = parts[1];
        String an = parts[2];

        // Obtener el mes actual
        Month mes = LocalDate.now().getMonth();
        // Convertir el numero del mes en nombre
        String nombremes = mes.getDisplayName(TextStyle.FULL, new Locale("es", "ES"));
        // Convierte a mayúscula la primera letra del mes.
        String primeraLetra = nombremes.substring(0,1);
        String mayuscula = primeraLetra.toUpperCase();
        String demasLetras = nombremes.substring(1, nombremes.length());
        nombremes = mayuscula + demasLetras;
        fecha.setText(di+" de "+nombremes+" del "+an);
        DisplayProgressDialog();

        //EdificiosAdapter adapter = new EdificiosAdapter(PrincipalActivity.this, LocalMock.getEdificios());
        //gridView.setAdapter(adapter);
        //gridView.setOnItemClickListener(PrincipalActivity.this);
        perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrincipalActivity.this, PerfilActivity.class);
                startActivity(intent);
                finish();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(PrincipalActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Tienes "+total+" registros sin guardar")
                        .setMessage("¿Seguro que desea subir los registros?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                subirRegistrosOffline();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        iniciarServicios();
    }

    private void iniciarServicios() {
        if(NetworkUtils.tieneAccesoInternet(PrincipalActivity.this)) {
            System.out.println("Con Internet");
            listarEdificios(token);
            consultarRegistrossinConecxion();
            inicializarListadosSinConexion();
        } else {
            System.out.println("Sin Internet");
            for (int i = 0; i < database.edificiosDao().getAll().size(); i++) {
                ResponseEdificios entity = new ResponseEdificios(
                        database.edificiosDao().getAll().get(i).getBuilding_id(),
                        database.edificiosDao().getAll().get(i).getName_building(),
                        database.edificiosDao().getAll().get(i).getImage_building(),
                        database.edificiosDao().getAll().get(i).getAddress());
                arrayedificiosoffline.add(entity);
            }

            EdificiosAdapter adapter = new EdificiosAdapter(PrincipalActivity.this, arrayedificiosoffline);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(PrincipalActivity.this);
        }
    }

    private void listarEdificios(String token) {
        pDialog.show();
        Call<ArrayList<ResponseEdificios>> obtenerEdificios = service.obtenerEdificios("Bearer " + token);
        obtenerEdificios.enqueue(new Callback<ArrayList<ResponseEdificios>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseEdificios>> call, Response<ArrayList<ResponseEdificios>> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("responseResumen: " + response);

                switch (response.code()){
                    case 200:
                        System.out.println(response.body().get(0).getBuilding_id());
                        EdificiosAdapter adapter = new EdificiosAdapter(PrincipalActivity.this, response.body());
                        gridView.setAdapter(adapter);
                        gridView.setOnItemClickListener(PrincipalActivity.this);
                        break;
                    case 400:
                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.apply();
                        Intent intent = new Intent(PrincipalActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                    case 404:
                        Toast.makeText(PrincipalActivity.this, "Usuario No Existe", Toast.LENGTH_SHORT).show();
                        break;
                    case 429:
                        SharedPreferences prefs1 = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = prefs1.edit();
                        editor1.clear();
                        editor1.apply();
                        Intent intent1 = new Intent(PrincipalActivity.this, LoginActivity.class);
                        startActivity(intent1);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseEdificios>> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                //Toast.makeText(PrincipalActivity.this, "Error Desconocido", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ResponseEdificios item = (ResponseEdificios) parent.getItemAtPosition(position);

        Intent intent = new Intent(this, MedicionActivity.class);
        intent.putExtra("id_edificio", String.valueOf(item.getBuilding_id()));
        intent.putExtra("nombre_edificio", item.getName_building());
        intent.putExtra("direccion_edificio", item.getAddress());
        startActivity(intent);
    }

    private void guardarListaEdificiosSinConexionSQL(List<ResponseEdificios> edificios) {
        edificiosDao = database.edificiosDao();
        if (existeListaEdificiosSinConexion()) {
            edificiosDao.deleteAll();
        }
        for (int i = 0; i < edificios.size(); i++) {
            EdificioEntity entity = new EdificioEntity(edificios.get(i), i);
            edificiosDao.insert(entity);
        }

        System.out.println("Guardando lista de edificios en bd interna");
    }

    private void guardarListaMedicionesSinConexionSQL(List<ResponseMedicionTipo> medicionTipos) {
        tipoMedicionDao = database.tipoMedicionDao();
        if (existeListaMedicionesSinConexion()) {
            tipoMedicionDao.deleteAll();
        }
        for (int i = 0; i < medicionTipos.size(); i++) {
            TipoMedicionEntity entity = new TipoMedicionEntity(medicionTipos.get(i), i);
            tipoMedicionDao.insert(entity);
        }

        System.out.println("Guardando lista de edificios en bd interna");
    }

    private boolean existeListaEdificiosSinConexion() {
        System.out.println("Consultando si existe lista de edificios guardada");
        return edificiosDao.getAll().size() > 0;
    }

    private boolean existeListaMedicionesSinConexion() {
        System.out.println("Existe lista de edificios sin conec");
        return tipoMedicionDao.getAll().size() > 0;
    }

    private void inicializarListadosSinConexion() {
        System.out.println("consultando para llenar lista");
        try {
            Call<ArrayList<ResponseEdificios>> obtenerEdificios = service.obtenerEdificios("Bearer " + token);
            obtenerEdificios.enqueue(new Callback<ArrayList<ResponseEdificios>>() {
                @Override
                public void onResponse(Call<ArrayList<ResponseEdificios>> call, Response<ArrayList<ResponseEdificios>> response) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    System.out.println("responseResumen: " + response);

                    if (response.code()== 200){
                        guardarListaEdificiosSinConexionSQL(response.body());
                        System.out.println(response.body().get(0).getBuilding_id());
                    }else if (response.code()==404){
                        //Toast.makeText(PrincipalActivity.this, "Usuario No Existe", Toast.LENGTH_SHORT).show();
                    }else{
                        //Toast.makeText(PrincipalActivity.this, "Error Desconocido", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<ResponseEdificios>> call, Throwable t) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    //Toast.makeText(PrincipalActivity.this, "Error Desconocido", Toast.LENGTH_SHORT).show();
                }
            });

            Call<RequestPadreResponseMedicionTipo> obtenerMedicion = service.obtenerMedicion("Bearer " + token);
            obtenerMedicion.enqueue(new Callback<RequestPadreResponseMedicionTipo>() {
                @Override
                public void onResponse(Call<RequestPadreResponseMedicionTipo> call, Response<RequestPadreResponseMedicionTipo> response) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    System.out.println("responseResumen: " + response);

                    if (response.code()== 200){
                        guardarListaMedicionesSinConexionSQL(response.body().getMeasurement_types());

                    }else if (response.code()==404){
                        Toast.makeText(PrincipalActivity.this, "Usuario No Existe", Toast.LENGTH_SHORT).show();
                        //mostrarToastFallido("Usuario No Existe");
                    }else{
                        //Toast.makeText(PrincipalActivity.this, "Error Desconocido", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<RequestPadreResponseMedicionTipo> call, Throwable t) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    //Toast.makeText(PrincipalActivity.this, "Error Desconocido", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Commons.mostrarDialogo(PrincipalActivity.this, "AVISO", "Ocurrio un error al iniciar la aplicación, por favor intentelo nuevamente.", true, "OK", false, "", null, 0);
        }
    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(PrincipalActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

    //consulta si hay registros guardados y si se encuentran en espera o subidos para actualizar
    private void consultarRegistrossinConecxion(){
        total = registroDao.getAll().size();
        if(total > 0){
             System.out.println("Tiene " + total + " registros guardados Offline");
             fab.setVisibility(View.VISIBLE);
        }else{
            fab.setVisibility(View.GONE);
            System.out.println("No hay registros sin enviar");
        }
    }

    private void subirRegistrosOffline(){
        int pendientes =0, enviados=0;

        /*try {
            System.out.println("Try PrincipalActivity");
            for (i = 0; i < total; i++) {
                //Verificar registros en espera para subirlos
                if(database.registroDao().getAll().get(i).getEstado() == 1){
                    System.out.println("Reg id: " +database.registroDao().getAll().get(i).getUid());
                    System.out.println("Reg nonth: " +database.registroDao().getAll().get(i).getReading_month_now());
                    System.out.println("Reg mesurementid: " +database.registroDao().getAll().get(i).getMeasurementid());
                    System.out.println("Reg estado: " +database.registroDao().getAll().get(i).getEstado() + "i:  " +i);
                    pDialog.show();
                    int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                    requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid,requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: "+response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                    registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                    Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(PrincipalActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(PrincipalActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    System.out.println("Sin registros pendiente de subida");
                }
            }

        }catch (Exception e){
            System.out.println("Catch PrincipalActivity: " + e);
            for (i = 0; i < total-1; i++) {

                *//*if(database.registroDao().getAll().get(i).getEstado() == 1){
                    pendientes++;
                    System.out.print("Registros pendientes: " + pendientes);
                } else {
                    enviados++;
                    System.out.print("Registros enviados: " +enviados);
                }*//*
                if(database.registroDao().getAll().get(i).getEstado() == 1){
                    System.out.println("Reg id: " +database.registroDao().getAll().get(i).getUid());
                    System.out.println("Reg nonth: " +database.registroDao().getAll().get(i).getReading_month_now());
                    System.out.println("Reg mesurementid: " +database.registroDao().getAll().get(i).getMeasurementid());
                    System.out.println("Reg estado: " +database.registroDao().getAll().get(i).getEstado() + "iiii:  " +i);
                    pDialog.show();
                    int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                    RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                    requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                    requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                    Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid,requestUpdateRegistro);
                    callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                        @Override
                        public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println("responseResumen: "+response);
                            switch (response.code()){
                                case 200:
                                    registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                    registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                    break;
                                case 422:
                                    System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                    Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 400:
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent2 = new Intent(PrincipalActivity.this, LoginActivity.class);
                                    startActivity(intent2);
                                    finish();
                                    break;
                                default:
                                    Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            System.out.println(getString(R.string.error_inesperado));
                            Toast.makeText(PrincipalActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    System.out.println("Sin registros pendiente de subida");
                }
            }
        }*/

        pDialogg = new ProgressDialog(PrincipalActivity.this);
        pDialogg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialogg.setMessage("Procesando...");
        pDialogg.setCancelable(false);
        //pDialogg.setMax(100);
        uploadOfflineAsincrona = new UploadOfflineAsincrona();
        uploadOfflineAsincrona.execute();
    }

    private void tareaLarga(){
        try {
            Thread.sleep(1000);
        } catch(InterruptedException e) {}
    }

    private class UploadOfflineAsincrona extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                System.out.println("Try PrincipalActivity");
                List<RegistroEntity> reg = database.registroDao().getAll();
                for(RegistroEntity registro: reg){
                    if (registro.getEstado() == 1) {
                        //tareaLarga();
                        //publishProgress(i*10);
                        System.out.println("Reg id fornew: " + registro.getUid());
                        System.out.println("Reg nonth fornew: " + registro.getReading_month_now());
                        System.out.println("Reg mesurementid fornew: " + registro.getMeasurementid());
                        //System.out.println("Photo: " + database.registroDao().getAll().get(i).getImage_attached());
                        System.out.println("Reg estado fornew: " + registro.getEstado());
                        //pDialog.show();
                        int measurementid = registro.getMeasurementid();
                        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                        requestUpdateRegistro.setReading_month_now(registro.getReading_month_now());
                        requestUpdateRegistro.setImage_attached(registro.getImage_attached());
                        Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                        callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                            @Override
                            public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                System.out.println("responseResumen: " + response);
                                switch (response.code()){
                                    case 200:
                                        registroDao.queryUpdate(2,registro.getUid());
                                        registroDao.queryDelete(registro.getUid());
                                        System.out.println("queryDelete: "+registro.getUid());
                                        System.out.println(response.body().getMessage());
                                        break;
                                    case 422:
                                        System.out.println("Registro "+registro.getUid()+" no se guardo.");
                                        Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 400:
                                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent2 = new Intent(PrincipalActivity.this, LoginActivity.class);
                                        startActivity(intent2);
                                        finish();
                                        break;
                                    default:
                                        Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                System.out.println(getString(R.string.error_inesperado));
                                Toast.makeText(PrincipalActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                /*for (i = 0; i < total; i++) {
                    if(database.registroDao().getAll().get(i).getEstado() == 1){
                        System.out.println("Reg id: " +database.registroDao().getAll().get(i).getUid());
                        System.out.println("Reg nonth: " +database.registroDao().getAll().get(i).getReading_month_now());
                        System.out.println("Reg mesurementid: " +database.registroDao().getAll().get(i).getMeasurementid());
                        System.out.println("Reg estado: " +database.registroDao().getAll().get(i).getEstado() + "i:  " +i);
                        pDialog.show();
                        int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                        requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                        requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                        Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid,requestUpdateRegistro);
                        callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                            @Override
                            public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                System.out.println("responseResumen: "+response);
                                switch (response.code()){
                                    case 200:
                                        registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                        registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                        System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                        System.out.println(response.body().getMessage());
                                        break;
                                    case 422:
                                        System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                        Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 400:
                                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent2 = new Intent(PrincipalActivity.this, LoginActivity.class);
                                        startActivity(intent2);
                                        finish();
                                        break;
                                    default:
                                        Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                System.out.println(getString(R.string.error_inesperado));
                                Toast.makeText(PrincipalActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        System.out.println("Sin registros pendiente de subida");
                    }
                }*/

            }catch (Exception e){
                System.out.println("Catch PrincipalActivity: " + e);
                for (i = 0; i < total-1; i++) {

                /*if(database.registroDao().getAll().get(i).getEstado() == 1){
                    pendientes++;
                    System.out.print("Registros pendientes: " + pendientes);
                } else {
                    enviados++;
                    System.out.print("Registros enviados: " +enviados);
                }*/
                    if(database.registroDao().getAll().get(i).getEstado() == 1){
                        System.out.println("Reg id: " +database.registroDao().getAll().get(i).getUid());
                        System.out.println("Reg nonth: " +database.registroDao().getAll().get(i).getReading_month_now());
                        System.out.println("Reg mesurementid: " +database.registroDao().getAll().get(i).getMeasurementid());
                        System.out.println("Reg estado: " +database.registroDao().getAll().get(i).getEstado() + "iiii:  " +i);
                        pDialog.show();
                        int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                        requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                        requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                        Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid,requestUpdateRegistro);
                        callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                            @Override
                            public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                System.out.println("responseResumen: "+response);
                                switch (response.code()){
                                    case 200:
                                        registroDao.queryUpdate(2,database.edificiosDao().getAll().get(i).getUid());
                                        registroDao.queryDelete(database.edificiosDao().getAll().get(i).getUid());
                                        System.out.println("queryDelete: "+database.edificiosDao().getAll().get(i).getUid());
                                        System.out.println(response.body().getMessage());
                                        break;
                                    case 422:
                                        System.out.println("Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.");
                                        Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 400:
                                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent2 = new Intent(PrincipalActivity.this, LoginActivity.class);
                                        startActivity(intent2);
                                        finish();
                                        break;
                                    default:
                                        Toast.makeText(PrincipalActivity.this, "Registro "+database.edificiosDao().getAll().get(i).getUid()+" no se guardo.", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                System.out.println(getString(R.string.error_inesperado));
                                Toast.makeText(PrincipalActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        System.out.println("Sin registros pendiente de subida");
                    }
                }
            }

            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialogg.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pDialogg.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    UploadOfflineAsincrona.this.cancel(true);
                }
            });
            pDialogg.setProgress(0);
            pDialogg.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                pDialogg.dismiss();
                Toast.makeText(PrincipalActivity.this, "¡Actualización de registros Off-Line finalizada!", Toast.LENGTH_SHORT).show();
                registroDao.deleteAll();
                consultarRegistrossinConecxion();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(PrincipalActivity.this, "¡Actualización de registros Off-Line cancelada!", Toast.LENGTH_SHORT).show();
        }
    }

}