package com.example.app_condominios.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.example.app_condominios.R;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.PermisionChecker;
import com.example.app_condominios.Util.Util;

import static com.example.app_condominios.Util.Constants.SPLASH_SCREEN;

public class SplashActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private String toke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        toke = prefs.getString(Constants.token,"");
        aceptarPermiso();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.INTENT_PERMISSION:
                String message = Util.onRequestPermissionsResult(permissions, grantResults);
                new Handler().postDelayed(new Runnable() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void run() {
                        try {
                            Intent intent;
                            if(toke.isEmpty()){
                                intent = new Intent(SplashActivity.this, LoginActivity.class);
                            }else{
                                intent = new Intent(SplashActivity.this, PrincipalActivity.class);
                            }
                            startActivity(intent);
                            finish();
                        } catch (Exception e) {

                        }

                    }
                }, SPLASH_SCREEN);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /*private void validarPermiso() {
        if (PermisionChecker.isCameraEnable(getApplicationContext())) {
            if (PermisionChecker.isWriteStorageExternalEnable(getApplicationContext())) {
                if (PermisionChecker.isRaedStorageExternalEnable(getApplicationContext())) {
                    System.out.println("Aqui 1");
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    System.out.println("Aqui 2");
                }
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                System.out.println("Aqui 3");
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMARA);
            System.out.println("Aqui 4");
        }

    }*/

    /*@Override
    protected void onPostResume() {
        super.onPostResume();
            if (PermisionChecker.isRaedStorageExternalEnable(SplashActivity.this)) {
                new Handler().postDelayed(new Runnable() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void run() {
                        try {
                            Intent intent;
                            if(toke.isEmpty()){
                                intent = new Intent(SplashActivity.this, LoginActivity.class);
                            }else{
                                intent = new Intent(SplashActivity.this, PrincipalActivity.class);
                            }
                            startActivity(intent);
                            finish();
                        } catch (Exception e) {
                        }

                    }
                }, SPLASH_SCREEN);
                //startActivity(new Intent(SplashActivity.this,MainActivity.class));
            } else {
                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
    }*/

   /* @Override
    protected void onResume() {
        super.onResume();
        //    getTokenGCM();
        if (PermisionChecker.isFineLocationAvaliable(getApplicationContext())) {

            if (PermisionChecker.isRaedStorageExternalEnable(SplashActivity.this)) {

            } else {
                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }


    }*/

    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (PermisionChecker.isRaedStorageExternalEnable(getApplicationContext())) {
                        //startActivity(new Intent(InicialActivity.this,opcionesDenunciaslActivity.class));
                    } else {
                        ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                }
                break;
            }

            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //startActivity(new Intent(InicialActivity.this,opcionesDenunciaslActivity.class));
                }
                break;
        }
    }*/

    private void aceptarPermiso() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permiso = Util.getPermisos(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA);
            if (permiso == null) {
                System.out.println("1");
                new Handler().postDelayed(new Runnable() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void run() {
                        try {
                            Intent intent;
                            if(toke.isEmpty()){
                                intent = new Intent(SplashActivity.this, LoginActivity.class);
                            }else{
                                intent = new Intent(SplashActivity.this, PrincipalActivity.class);
                            }
                            startActivity(intent);
                            finish();
                        } catch (Exception e) {
                        }

                    }
                }, SPLASH_SCREEN);
            } else {
                requestPermissions(permiso, Constants.INTENT_PERMISSION);
            }
        } else{
            System.out.println("2");
            new Handler().postDelayed(new Runnable() {
                @SuppressLint("MissingPermission")
                @Override
                public void run() {
                    try {
                        Intent intent;
                        if(toke.isEmpty()){
                            intent = new Intent(SplashActivity.this, LoginActivity.class);
                        }else{
                            intent = new Intent(SplashActivity.this, PrincipalActivity.class);
                        }
                        startActivity(intent);
                        finish();
                    } catch (Exception e) {
                    }

                }
            }, SPLASH_SCREEN);
        }
    }

}