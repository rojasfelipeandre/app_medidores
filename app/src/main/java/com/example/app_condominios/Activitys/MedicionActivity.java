package com.example.app_condominios.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_condominios.Adapters.EdificiosAdapter;
import com.example.app_condominios.Adapters.MedicionesAdapter;
import com.example.app_condominios.Database.AppDatabase;
import com.example.app_condominios.Database.dao.MedicionDepartDao;
import com.example.app_condominios.Database.dao.MedicionDepartDao2;
import com.example.app_condominios.Database.dao.RegistroDao;
import com.example.app_condominios.Database.entity.MedicionDepartEntity;
import com.example.app_condominios.Database.entity.MedicionDepartEntity2;
import com.example.app_condominios.Database.entity.RegistroEntity;
import com.example.app_condominios.Models.Request.RequestPadreResponseMedicionTipo;
import com.example.app_condominios.Models.Request.RequestUpdateRegistro;
import com.example.app_condominios.Models.Response.ResponseMedicionDepart;
import com.example.app_condominios.Models.Response.ResponseMedicionTipo;
import com.example.app_condominios.Models.Response.ResponseRegistroMedicion;
import com.example.app_condominios.R;
import com.example.app_condominios.Service.Api;
import com.example.app_condominios.Service.ApiServices;
import com.example.app_condominios.Util.Commons;
import com.example.app_condominios.Util.Constants;
import com.example.app_condominios.Util.ExpandableHeightGridView;
import com.example.app_condominios.Util.RedInternet.ConexionServicio;
import com.example.app_condominios.Util.RedInternet.ConexionViewModel;
import com.example.app_condominios.Util.RedInternet.NetworkUtils;
import com.example.app_condominios.Util.RedInternet.ServicioConectado;
import com.example.app_condominios.Util.RedInternet.ServicioDesconectado;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicionActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ApiServices service = Api.getConfigurationApi().create(ApiServices.class);
    private SharedPreferences prefs;
    private ImageView atras;
    private TextView textTitulo,textdireccion;
    private ExpandableHeightGridView gridView;
    private String idd, nombre,direccion;
    private  ProgressDialog pDialog;
    AppDatabase database;
    MedicionDepartDao medicionDepartDao;
    MedicionDepartDao2 medicionDepartDao2;
    RegistroDao registroDao;
    String imagen;
    double medicion;
    private int i, idtab, idestado, iddepa;
    private ArrayList<ResponseMedicionTipo> arraymedicionOffline = new ArrayList<>();
    private String token, mes, ano;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicion);
        changeStatusBarColor();
        database = AppDatabase.getDatabaseInstance(MedicionActivity.this);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        token = prefs.getString(Constants.token,"");
        idd = getIntent().getStringExtra("id_edificio");
        nombre = getIntent().getStringExtra("nombre_edificio");
        direccion = getIntent().getStringExtra("direccion_edificio");
        atras = findViewById(R.id.atras);
        textTitulo = findViewById(R.id.textTitulo);
        textdireccion = findViewById(R.id.textdireccion);
        gridView = findViewById(R.id.gridview);
        gridView.setExpanded(true);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtils.tieneAccesoInternet(MedicionActivity.this)) {
                    onBackPressed();
                }else{
                    Toast.makeText(MedicionActivity.this, "No puede cambiar de edificio en modo Off-Line", Toast.LENGTH_SHORT).show();
                }
            }
        });
        textTitulo.setText("¿Que tipo de medición deseas realizar en "+nombre+"?");
        textdireccion.setText(direccion);
        //Obtener fehca
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        //Separar dia y año
        String[] parts = formattedDate.split("-");
        String di = parts[0];
        mes = parts[1];
        ano = parts[2];
        DisplayProgressDialog();
        iniciarServicios();
    }

    private void iniciarServicios() {
        if(NetworkUtils.tieneAccesoInternet(MedicionActivity.this)) {
            System.out.println("Con Internet");
            listarMediciones(token);
            //consultarRegistrossinConecxion();
            inicializarListadosDepaSinConexion();
        } else {
            System.out.println("Sin Internet");
            for (int i = 0; i < database.tipoMedicionDao().getAll().size(); i++) {
                ResponseMedicionTipo entity = new ResponseMedicionTipo(
                        database.tipoMedicionDao().getAll().get(i).getMeasurement_type_id(),
                        database.tipoMedicionDao().getAll().get(i).getCode_measurement(),
                        database.tipoMedicionDao().getAll().get(i).getName_measurement_type(),
                        database.tipoMedicionDao().getAll().get(i).getImage_measurement());
                arraymedicionOffline.add(entity);
            }

            MedicionesAdapter adapter = new MedicionesAdapter(MedicionActivity.this, arraymedicionOffline);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(MedicionActivity.this);
        }
    }

    private void listarMediciones(String token) {
        pDialog.show();
        Call<RequestPadreResponseMedicionTipo> obtenerMedicion = service.obtenerMedicion("Bearer " + token);
        obtenerMedicion.enqueue(new Callback<RequestPadreResponseMedicionTipo>() {
            @Override
            public void onResponse(Call<RequestPadreResponseMedicionTipo> call, Response<RequestPadreResponseMedicionTipo> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("responseResumen: " + response);

                switch (response.code()){
                    case 200:
                        MedicionesAdapter adapter = new MedicionesAdapter(MedicionActivity.this, response.body().getMeasurement_types());
                        gridView.setAdapter(adapter);
                        gridView.setOnItemClickListener(MedicionActivity.this);
                        break;
                    case 400:
                        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.apply();
                        Intent intent = new Intent(MedicionActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                    case 404:
                        Toast.makeText(MedicionActivity.this, "Error al obtener Tipos de mediciones", Toast.LENGTH_SHORT).show();
                        break;
                    case 429:
                        SharedPreferences prefs1 = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = prefs1.edit();
                        editor1.clear();
                        editor1.apply();
                        Intent intent1 = new Intent(MedicionActivity.this, LoginActivity.class);
                        startActivity(intent1);
                        finish();
                        System.out.println(getString(R.string.error_inesperado));
                        break;
                }

            }

            @Override
            public void onFailure(Call<RequestPadreResponseMedicionTipo> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(MedicionActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                System.out.println(getString(R.string.error_inesperado));
                //Toast.makeText(MedicionActivity.this, "Error Desconocido", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ResponseMedicionTipo item = (ResponseMedicionTipo) parent.getItemAtPosition(position);

        Intent intent = new Intent(MedicionActivity.this, MedicionDepartamentoActivity.class);
        intent.putExtra("id_medicion", String.valueOf(item.getMeasurement_type_id()));
        intent.putExtra("nombre_medicion", item.getName_measurement_type());
        intent.putExtra("id_edificio", idd);
        intent.putExtra("nombre_edificio", nombre);
        intent.putExtra("mesnum", "0");
        intent.putExtra("mes","");
        intent.putExtra("anore","");
        System.out.println(item.getMeasurement_type_id() +" "+ item.getName_measurement_type()+" "+idd);
        startActivity(intent);
        finish();
    }

    private void inicializarListadosDepaSinConexion() {
        String param1 = idd;
        String param2 = "2";
        String param4 = "1";
        String param3 = mes+"-"+ano;

        System.out.println("consultando para llenar lista");
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("building_id", param1)
                    .addFormDataPart("measurement_type_id", param2)
                    .addFormDataPart("code_period", param3)
                    .build();

            System.out.println(param1+" "+param2+" "+param3+" "+token);
            Call<ArrayList<ResponseMedicionDepart>> callSelecconarCliente = service.obtenerMedicionDepa("Bearer " + token, requestBody);
            callSelecconarCliente.enqueue(new Callback<ArrayList<ResponseMedicionDepart>>() {
                @Override
                public void onResponse(Call<ArrayList<ResponseMedicionDepart>> call, Response<ArrayList<ResponseMedicionDepart>> response) {
                    System.out.println("responseResumen: "+response);
                    if (response.code()== 200) {
                        guardarListaLuzSinConexionSQL(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ResponseMedicionDepart>> call, Throwable t) {
                    Toast.makeText(MedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                }
            });


            RequestBody requestBody2 = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("building_id", param1)
                    .addFormDataPart("measurement_type_id", param4)
                    .addFormDataPart("code_period", param3)
                    .build();

            System.out.println(param1+" "+param2+" "+param3+" "+token);
            Call<ArrayList<ResponseMedicionDepart>> callSelecconarCliente2 = service.obtenerMedicionDepa("Bearer " + token, requestBody2);
            callSelecconarCliente2.enqueue(new Callback<ArrayList<ResponseMedicionDepart>>() {
                @Override
                public void onResponse(Call<ArrayList<ResponseMedicionDepart>> call, Response<ArrayList<ResponseMedicionDepart>> response) {
                    System.out.println("responseResumen: "+response);
                    if (response.code()== 200) {
                        guardarListaAguaSinConexionSQL(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ResponseMedicionDepart>> call, Throwable t) {
                    Toast.makeText(MedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                }
            });



        } catch (Exception e) {
            Commons.mostrarDialogo(MedicionActivity.this, "AVISO", "Ocurrio un error al iniciar la aplicación, por favor intentelo nuevamente.", true, "OK", false, "", null, 0);
        }
    }

    private void guardarListaLuzSinConexionSQL(List<ResponseMedicionDepart> ListaLuz) {
        medicionDepartDao = database.medicionDepartDao();
        if (existeListaLuzSinConexion()) {
            medicionDepartDao.deleteAll();
        }
        for (int i = 0; i < ListaLuz.size(); i++) {
            MedicionDepartEntity entity = new MedicionDepartEntity(ListaLuz.get(i), i);
            medicionDepartDao.insert(entity);
        }

        System.out.println("Guardando lista de Luz en bd interna");
    }

    private void guardarListaAguaSinConexionSQL(List<ResponseMedicionDepart> ListaAgua) {
        medicionDepartDao2 = database.medicionDepartDao2();
        if (existeListaAguaSinConexion()) {
            medicionDepartDao2.deleteAll();
        }
        for (int i = 0; i < ListaAgua.size(); i++) {
            MedicionDepartEntity2 entity = new MedicionDepartEntity2(ListaAgua.get(i), i);
            medicionDepartDao2.insert(entity);
        }

        System.out.println("Guardando lista de Agua en bd interna");
    }

    private boolean existeListaLuzSinConexion() {
        System.out.println("Existe lista de Luz sin conec");
        return medicionDepartDao.getAll().size() > 0;
    }

    private boolean existeListaAguaSinConexion() {
        System.out.println("Existe lista de Agua sin conec");
        return medicionDepartDao2.getAll().size() > 0;
    }

    public void DisplayProgressDialog() {
        pDialog = new ProgressDialog(MedicionActivity.this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

    /*private void consultarRegistrossinConecxion(){
        registroDao = database.registroDao();
        int total = registroDao.getAll().size();
        int enviados = 0, pendientes = 0;
        if(total > 0){
            System.out.println("Si hay registros son : " + total);

            try {
                System.out.println("Try -1");

                for (i = 0; i < database.registroDao().getAll().size()-1; i++) {

                    if (database.registroDao().getAll().get(i).getEstado() == 1) {
                        pendientes++;
                        System.out.print("Registros pendientes: " + pendientes);
                    } else {
                        enviados++;
                        System.out.print("Registros enviados: " + enviados);
                    }

                    //Verificar registros en espera para subirlos
                    if (database.registroDao().getAll().get(i).getEstado() == 1) {

                        System.out.println(" Reg id: " + database.edificiosDao().getAll().get(i).getUid());
                        System.out.println("Reg nonth: " + database.registroDao().getAll().get(i).getReading_month_now());
                        //System.out.println("Reg atached: " +imagen);
                        System.out.println("Reg mesurementid: " + database.registroDao().getAll().get(i).getMeasurementid());
                        System.out.println("Reg estado: " + database.registroDao().getAll().get(i).getEstado());

                        int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                        requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                        requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                        Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                        callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                            @Override
                            public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                                System.out.println("responseResumen: " + response);
                                if (response.code() == 200) {
                                    registroDao.queryUpdate(2, database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                } else {
                                    System.out.println(getString(R.string.error_inesperado));
                                    //Toast.makeText(MedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                                System.out.println(getString(R.string.error_inesperado));
                                //Toast.makeText(MedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

            } catch (Exception e){

                System.out.println("Catch 0: " + e);

                for (i = 0; i < database.registroDao().getAll().size(); i++) {

                    if (database.registroDao().getAll().get(i).getEstado() == 1) {
                        pendientes++;
                        System.out.print("Registros pendientes: " + pendientes);
                    } else {
                        enviados++;
                        System.out.print("Registros enviados: " + enviados);
                    }

                    //Verificar registros en espera para subirlos
                    if (database.registroDao().getAll().get(i).getEstado() == 1) {

                        System.out.println(" Reg id: " + database.edificiosDao().getAll().get(i).getUid());
                        System.out.println("Reg nonth: " + database.registroDao().getAll().get(i).getReading_month_now());
                        //System.out.println("Reg atached: " +imagen);
                        System.out.println("Reg mesurementid: " + database.registroDao().getAll().get(i).getMeasurementid());
                        System.out.println("Reg estado: " + database.registroDao().getAll().get(i).getEstado());

                        int measurementid = database.registroDao().getAll().get(i).getMeasurementid();
                        RequestUpdateRegistro requestUpdateRegistro = new RequestUpdateRegistro();
                        requestUpdateRegistro.setReading_month_now(database.registroDao().getAll().get(i).getReading_month_now());
                        requestUpdateRegistro.setImage_attached(database.registroDao().getAll().get(i).getImage_attached());
                        Call<ResponseRegistroMedicion> callSelecconarCliente = service.actualizarMedicion("Bearer " + token, measurementid, requestUpdateRegistro);
                        callSelecconarCliente.enqueue(new Callback<ResponseRegistroMedicion>() {
                            @Override
                            public void onResponse(Call<ResponseRegistroMedicion> call, Response<ResponseRegistroMedicion> response) {
                                System.out.println("responseResumen: " + response);
                                if (response.code() == 200) {
                                    registroDao.queryUpdate(2, database.edificiosDao().getAll().get(i).getUid());
                                    System.out.println(response.body().getMessage());
                                } else {
                                    System.out.println(getString(R.string.error_inesperado));
                                    //Toast.makeText(MedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRegistroMedicion> call, Throwable t) {
                                System.out.println(getString(R.string.error_inesperado));
                                //Toast.makeText(MedicionActivity.this, getString(R.string.error_inesperado), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

                }
        }else{
            System.out.println("No hay registros sin enviar");
        }
    }*/

    @Override
    public void onBackPressed() {
        if(NetworkUtils.tieneAccesoInternet(MedicionActivity.this)) {
            super.onBackPressed();
        }else{
            Toast.makeText(MedicionActivity.this, "No puede cambiar de edificio en modo OffLine", Toast.LENGTH_SHORT).show();
        }
    }

}