package com.example.app_condominios.Models.Request;

public class RequestUpdateRegistro {
    private double reading_month_now;
    private String image_attached;
    private String token;

    public RequestUpdateRegistro() {}

    public RequestUpdateRegistro(double reading_month_now, String image_attached, String token) {
        this.reading_month_now = reading_month_now;
        this.image_attached = image_attached;
        this.token = token;
    }

    public double getReading_month_now() {
        return reading_month_now;
    }

    public void setReading_month_now(double reading_month_now) {
        this.reading_month_now = reading_month_now;
    }

    public String getImage_attached() {
        return image_attached;
    }

    public void setImage_attached(String image_attached) {
        this.image_attached = image_attached;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
