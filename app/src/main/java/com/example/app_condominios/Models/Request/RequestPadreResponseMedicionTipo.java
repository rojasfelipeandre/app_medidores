package com.example.app_condominios.Models.Request;

import com.example.app_condominios.Models.Response.ResponseMedicionTipo;

import java.util.ArrayList;

public class RequestPadreResponseMedicionTipo {

    private ArrayList<ResponseMedicionTipo> measurement_types;

    public RequestPadreResponseMedicionTipo() {}

    public RequestPadreResponseMedicionTipo(ArrayList<ResponseMedicionTipo> measurement_types) {
        this.measurement_types = measurement_types;
    }

    public ArrayList<ResponseMedicionTipo> getMeasurement_types() {
        return measurement_types;
    }

    public void setMeasurement_types(ArrayList<ResponseMedicionTipo> measurement_types) {
        this.measurement_types = measurement_types;
    }
}
