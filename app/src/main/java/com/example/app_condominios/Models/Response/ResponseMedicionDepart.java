package com.example.app_condominios.Models.Response;

public class ResponseMedicionDepart {

    private int measurement_id;
    private String number_estate;
    private String reading_month_past;
    private String reading_month_now;
    private String image_attached;
    // 0 permitirse aún la edición  - 1 bloquear la edición
    private int is_closed;

    public ResponseMedicionDepart() {}

    public ResponseMedicionDepart(int measurement_id, String number_estate, String reading_month_past, String reading_month_now, String image_attached, int is_closed) {
        this.measurement_id = measurement_id;
        this.number_estate = number_estate;
        this.reading_month_past = reading_month_past;
        this.reading_month_now = reading_month_now;
        this.image_attached = image_attached;
        this.is_closed = is_closed;
    }



    public int getMeasurement_id() {
        return measurement_id;
    }

    public void setMeasurement_id(int measurement_id) {
        this.measurement_id = measurement_id;
    }

    public String getNumber_estate() {
        return number_estate;
    }

    public void setNumber_estate(String number_estate) {
        this.number_estate = number_estate;
    }

    public String getReading_month_past() {
        return reading_month_past;
    }

    public void setReading_month_past(String reading_month_past) {
        this.reading_month_past = reading_month_past;
    }

    public String getReading_month_now() {
        return reading_month_now;
    }

    public void setReading_month_now(String reading_month_now) {
        this.reading_month_now = reading_month_now;
    }

    public String getImage_attached() {
        return image_attached;
    }

    public void setImage_attached(String image_attached) {
        this.image_attached = image_attached;
    }

    public int getIs_closed() {
        return is_closed;
    }

    public void setIs_closed(int is_closed) {
        this.is_closed = is_closed;
    }
}
