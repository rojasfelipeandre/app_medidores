package com.example.app_condominios.Models.Response;

import com.example.app_condominios.Database.entity.EdificioEntity;

public class ResponseEdificios {

    private int building_id;
    private String name_building;
    private String image_building;
    private String address;

    public ResponseEdificios(EdificioEntity edificioEntity, int i) {}

    public ResponseEdificios(int building_id, String name_building, String image_building, String address) {
        this.building_id = building_id;
        this.name_building = name_building;
        this.image_building = image_building;
        this.address = address;
    }

    public int getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(int building_id) {
        this.building_id = building_id;
    }

    public String getName_building() {
        return name_building;
    }

    public void setName_building(String name_building) {
        this.name_building = name_building;
    }

    public String getImage_building() {
        return image_building;
    }

    public void setImage_building(String image_building) {
        this.image_building = image_building;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
