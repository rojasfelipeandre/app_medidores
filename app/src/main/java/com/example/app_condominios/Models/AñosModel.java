package com.example.app_condominios.Models;

public class AñosModel {
    private String año;

    public AñosModel() {}

    public AñosModel(String año) {
        this.año = año;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }
}
