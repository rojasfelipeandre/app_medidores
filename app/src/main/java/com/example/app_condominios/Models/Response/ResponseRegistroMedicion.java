package com.example.app_condominios.Models.Response;

public class ResponseRegistroMedicion {

    private  String message;

    public ResponseRegistroMedicion() {}

    public ResponseRegistroMedicion(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
