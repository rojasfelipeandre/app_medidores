package com.example.app_condominios.Models.Response;

public class ResponseMedicionTipo {

    private int measurement_type_id;
    private String code_measurement;
    private String name_measurement_type;
    private String image_measurement;

    public ResponseMedicionTipo() {}

    public ResponseMedicionTipo(int measurement_type_id, String code_measurement, String name_measurement_type, String image_measurement) {
        this.measurement_type_id = measurement_type_id;
        this.code_measurement = code_measurement;
        this.name_measurement_type = name_measurement_type;
        this.image_measurement = image_measurement;
    }

    public int getMeasurement_type_id() {
        return measurement_type_id;
    }

    public void setMeasurement_type_id(int measurement_type_id) {
        this.measurement_type_id = measurement_type_id;
    }

    public String getCode_measurement() {
        return code_measurement;
    }

    public void setCode_measurement(String code_measurement) {
        this.code_measurement = code_measurement;
    }

    public String getName_measurement_type() {
        return name_measurement_type;
    }

    public void setName_measurement_type(String name_measurement_type) {
        this.name_measurement_type = name_measurement_type;
    }

    public String getImage_measurement() {
        return image_measurement;
    }

    public void setImage_measurement(String image_measurement) {
        this.image_measurement = image_measurement;
    }
}
